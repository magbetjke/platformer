/**
 *
 * Water.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include "cocos2d.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

class Water : public cocos2d::Sprite3D
{
public:
    CREATE_FUNC(Water)
    
    static const std::string WATER_SHADER;
    
    static Water* create(const cocos2d::Vec2& value);

    CC_PROPERTY(float, _step, Step);
    CC_PROPERTY(int, _rows, Rows);
    CC_PROPERTY(int, _cols, Cols);
    CC_PROPERTY_PASS_BY_REF(cocos2d::Vec2, _gridSize, GridSize);
    CC_PROPERTY(GLenum, _primitiveType, PrimitiveType);
    CC_PROPERTY(float, _uvRepeatRatio, UVRepeatRatio);
    CC_SYNTHESIZE_PASS_BY_REF(cocos2d::Vec2, _direction, WaveDirection);
    CC_SYNTHESIZE(float, _amplitude, WaveAmplitude);
    CC_SYNTHESIZE(float, _uvSpeed, UVSpeed);
    CC_PROPERTY(float, _length, WaveLength);
    CC_PROPERTY(float, _speed, WaveSpeed);
    CC_SYNTHESIZE(float, _steepness, WaveStepness);
    void setTexture(const std::string& path);
    
    Water();
    virtual ~Water();
    

    void update(float delta);
protected:
    void onEnter() override;
    void onExit() override;
    bool init() override;

    virtual void visit(cocos2d::Renderer *renderer, const cocos2d::Mat4& parentTransform, uint32_t parentFlags) override;
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    
    float _frequency;
    float _phase;
    
private:
    void buildMesh();
    void buildQuad(const cocos2d::Vec2& tl, const float& step, const cocos2d::Vec4& uv, std::vector<float>& positions, std::vector<float>& normals, std::vector<float>& texs, cocos2d::Mesh::IndexArray& indices);
    
    void applyVertexAttribs();
    void updateVolume();
    
    std::string _texturePath;
    cocos2d::Vec2 _uvOffset;

    cocos2d::Mesh* _mesh;
    cocos2d::GLProgramState* _state;
    
    bool _sizeIsDirty;
    bool _gridIsDirty;

};