uniform vec3 u_DirLightSourceColor[1];
uniform vec3 u_DirLightSourceDirection[1];

uniform vec3 u_AmbientLightSourceColor;

uniform vec4 u_color;

void main(void)
{
    gl_FragColor = u_color;
}