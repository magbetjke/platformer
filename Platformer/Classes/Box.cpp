/**
 *
 * Box.cpp
 *
 * @author a.eremin
 *
 */

#include "Box.hpp"
#include <math.h>

using namespace cocos2d;

const std::string Box::BOX_SHADER = "boxShader";


Box* Box::create(const Vec3& value)
{
    Box *pRet = new(std::nothrow) Box();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        pRet->setVolume(value);
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

void Box::setVolume(const Vec3& value)
{
    if (_volumeSize != value)
    {
        _volumeSize = value;
        _sizeIsDirty = true;
    }
}

Box::Box()
    :_mesh(nullptr)
    ,_programState(nullptr)
    ,_volumeSize(Vec3{10.0f, 10.0f, 10.0f})
    ,_sizeIsDirty(true)
{
}

Box::~Box()
{
    CC_SAFE_RELEASE(_mesh);
}

bool Box::init()
{
    if (Sprite3D::init())
    {
        buildMesh();
        
        _mesh->retain();
        this->addMesh(_mesh);
        
        applyShader();
        
//        _shadow = PlanarShadow::create();
//        _shadow->addOccluder(_mesh);
//        _shadow->setColor(Color3B::GRAY);
//        addChild(_shadow);
        
        return true;
    }
    
    return false;
}

void Box::visit(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    if (_sizeIsDirty)
    {
        updateVolume();
        
        _sizeIsDirty = false;
    }
    
    Sprite3D::visit(renderer, transform, flags);
    
}

void Box::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    //todo batch
    Sprite3D::draw(renderer, transform, flags);
}

void Box::buildMesh()
{
    std::vector<float> positions;
    std::vector<float> normals;
    std::vector<float> texs;
    Mesh::IndexArray indices;
    
    Vec3 vert;
    //front face
    {
        positions.push_back(-5.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(5.0f);positions.push_back(5.0f);
        positions.push_back(-5.0f);positions.push_back(5.0f);positions.push_back(5.0f);
        
        
        
        for (int i = 0; i != 4; i++)
        {
             normals.push_back(0.0f);normals.push_back(0.0f);normals.push_back(1.0f);
        }
        
        texs.push_back(0.0f);texs.push_back(0.5f);
        texs.push_back(0.335973f);texs.push_back(0.5f);
        texs.push_back(0.335973f);texs.push_back(0.0f);
        texs.push_back(0.0f);texs.push_back(0.0f);
        
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);
        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);
    }
    
     //top face
    {
        positions.push_back(-5.0f);positions.push_back(5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
        positions.push_back(-5.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
        
        
        for (int i = 0; i != 4; i++)
        {
             normals.push_back(0.0f);normals.push_back(1.0f);normals.push_back(0.0f);
        }
        
        texs.push_back(0.335973f);texs.push_back(0.5f);
        texs.push_back(0.667969f);texs.push_back(0.5f);
        texs.push_back(0.667969f);texs.push_back(0.0f);
        texs.push_back(0.335973f);texs.push_back(0.0f);
        
        indices.push_back(4);
        indices.push_back(5);
        indices.push_back(6);
        indices.push_back(4);
        indices.push_back(6);
        indices.push_back(7);
    }
    
     //bottom face
    {
        positions.push_back(-5.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
        positions.push_back(-5.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
        
        
        for (int i = 0; i != 4; i++)
        {
             normals.push_back(0.0f);normals.push_back(-1.0f);normals.push_back(0.0f);
        }
        
        texs.push_back(0.667969f);texs.push_back(0.5f);
        texs.push_back(1.0f);texs.push_back(0.5f);
        texs.push_back(1.0f);texs.push_back(0.0f);
        texs.push_back(0.667969f);texs.push_back(0.0f);
        
        indices.push_back(8);
        indices.push_back(10);
        indices.push_back(9);
        
        indices.push_back(8);
        
        indices.push_back(11);
        indices.push_back(10);
    }
    
     //back face
    {
        positions.push_back(-5.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
        positions.push_back(5.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
        positions.push_back(5.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
        positions.push_back(-5.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
        
        
        for (int i = 0; i != 4; i++)
        {
             normals.push_back(0.0f);normals.push_back(0.0f);normals.push_back(-1.0f);
        }
        
        texs.push_back(0.0f);texs.push_back(1.0f);
        texs.push_back(0.335973f);texs.push_back(1.0f);
        texs.push_back(0.335973f);texs.push_back(0.0f);
        texs.push_back(0.0f);texs.push_back(0.0f);;
        
        indices.push_back(12);
        indices.push_back(14);
        indices.push_back(13);
        indices.push_back(12);
        indices.push_back(15);
        indices.push_back(14);
    }
    
     //left face
    {
        positions.push_back(-5.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
        positions.push_back(-5.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
        positions.push_back(-5.0f);positions.push_back(5.0f);positions.push_back(5.0f);
        positions.push_back(-5.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
        
        
        for (int i = 0; i != 4; i++)
        {
             normals.push_back(-1.0f);normals.push_back(0.0f);normals.push_back(0.0f);
        }
        
        texs.push_back(0.0f);texs.push_back(1.0f);
        texs.push_back(0.667969f);texs.push_back(1.0f);
        texs.push_back(0.667969f);texs.push_back(0.0f);
        texs.push_back(0.0f);texs.push_back(0.0f);
        
        indices.push_back(16);
        indices.push_back(17);
        indices.push_back(18);
        indices.push_back(16);
        indices.push_back(18);
        indices.push_back(19);
    }
    
    //right face
    {
        positions.push_back(5.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
        positions.push_back(5.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(5.0f);positions.push_back(5.0f);
        positions.push_back(5.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
        
        
        for (int i = 0; i != 4; i++)
        {
             normals.push_back(1.0f);normals.push_back(0.0f);normals.push_back(0.0f);
        }
        
        texs.push_back(0.0f);texs.push_back(1.0f);
        texs.push_back(1.0f);texs.push_back(1.0f);
        texs.push_back(1.0f);texs.push_back(0.0f);
        texs.push_back(0.0f);texs.push_back(0.0f);
        
        indices.push_back(20);
        indices.push_back(22);
        indices.push_back(21);
        indices.push_back(20);
        indices.push_back(23);
        indices.push_back(22);
    }
    
    _mesh = Mesh::create(positions, normals, texs, indices);
}

GLProgram* Box::getOrCreateShader()
{
    GLProgram* shader = GLProgramCache::getInstance()->getGLProgram(BOX_SHADER);
    
    if (!shader)
    {
        shader = GLProgram::createWithFilenames("shaders/box.vert", "shaders/box.frag");
    }
    
    return shader;
}

void Box::applyShader()
{
    _shaderUsingLight = true;
    
    _programState = GLProgramState::create(getOrCreateShader());
    setGLProgramState(_programState);
        
    auto attributeCount = _mesh->getMeshVertexAttribCount();
    
    long offset = 0;
    for (auto i = 0; i < attributeCount; i++) {
        auto meshattribute = _mesh->getMeshVertexAttribute(i);
        _programState->setVertexAttribPointer(s_attributeNames[meshattribute.vertexAttrib],
            meshattribute.size,
            meshattribute.type,
            GL_FALSE,
            _mesh->getVertexSizeInBytes(),
            (GLvoid*)offset);
        offset += meshattribute.attribSizeBytes;
    }
}

void Box::updateVolume()
{
    auto aabb = getAABB();
    auto size = aabb._max - aabb._min;
    
    float scale_x = _volumeSize.x / size.x;
    float scale_y = _volumeSize.y / size.y;
    float scale_z = _volumeSize.z / size.z;
    
    setScaleX(getScaleX() * scale_x);
    setScaleY(getScaleY() * scale_y);
    setScaleZ(getScaleZ() * scale_z);
    
    _radius_xy = sqrt(_volumeSize.x * _volumeSize.x + _volumeSize.y * _volumeSize.y) * 0.5f;
}