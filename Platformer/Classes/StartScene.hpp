//
//  StartScreen.h
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#pragma once

#include "cocos2d.h"

class StartScene final : public cocos2d::Layer
{
public:
    CREATE_FUNC(StartScene)
    static cocos2d::Scene* createScene();
    
    ~StartScene();
protected:
    bool init() override;
    
private:
    cocos2d::DirectionLight* _light;
    cocos2d::AmbientLight* _ambient;

};