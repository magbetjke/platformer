//
//  GameOverView.cpp
//  Platformer
//
//  Created by Алексей Еремин on 04/05/15.
//
//

#include "GameOverView.hpp"
#include "AnimatedDigits.hpp"

using namespace cocos2d;
using namespace cocos2d::ui;

GameOverView::GameOverView()
    :_score(nullptr)
    ,_onClose(nullptr)
    ,_shown(false)
{
}

GameOverView::~GameOverView()
{
    Director::getInstance()->getEventDispatcher()->removeEventListener(_touchListener);
    CC_SAFE_RELEASE(_touchListener);
}

void GameOverView::initRenderer()
{
    Layout::initRenderer();
    
    auto size = Director::getInstance()->getVisibleSize();

    auto bg = Sprite::create();
    bg->setContentSize(size);
    bg->setTextureRect(Rect(0, 0, size.width, size.height));
    bg->setColor(Color3B::BLACK);
    bg->setOpacity(150.0f);
    
    addChild(bg);

    setCascadeOpacityEnabled(true);
            
    setContentSize(size);
    
    auto container = VBox::create();
    container->setCascadeOpacityEnabled(true);
    
    container->setPositionY(size.height * 0.3f);
    
    LinearLayoutParameter* lp = LinearLayoutParameter::create();
    lp->setGravity(LinearGravity::CENTER_HORIZONTAL);
    
    auto your_score = Text::create("Your score", "fonts/arial.ttf", 50);
    your_score->setLayoutParameter(lp);
    
    _score = AnimatedDigits::create("fonts/arial.ttf", 50);

    _score->setLayoutParameter(lp);
    
    _coins = Text::create("", "fonts/arial.ttf", 50);
    _coins->setColor(Color3B::YELLOW);
    _coins->setLayoutParameter(lp);
    
    auto lp2 = LinearLayoutParameter::create();
    lp2->setGravity(LinearGravity::CENTER_HORIZONTAL);
    lp2->setMargin(Margin(0, 30, 0, 0));
    _tapToRestart = Text::create("TAP TO RESTART", "fonts/arial.ttf", 25);
    _tapToRestart->setLayoutParameter(lp2);
    
    addChild(container);
    
    container->addChild(your_score);
    container->addChild(_score);
    container->addChild(_coins);
    container->addChild(_tapToRestart);
    
    setVisible(false);
    
    _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->onTouchBegan = CC_CALLBACK_2(GameOverView::onTouchBegan, this);
    _touchListener->retain();
}

void GameOverView::show(int score, int coins)
{
    if (_shown) return;
    
    _shown = true;
    
    _tapToRestart->setVisible(false);
    
    _coins->setString(std::to_string(coins));
    
    setOpacity(0);
    
    auto fade_in = FadeIn::create(1.5f);
    
    _score->setStartNumber(0);
    _score->setFinishNumber(score);
    
    stopAllActions();
    runAction(Sequence::createWithTwoActions(
            fade_in,
            CallFunc::create(CC_CALLBACK_0(GameOverView::onShow, this))
        )
    );
    
    setVisible(true);
}

void GameOverView::hide()
{
    if (!_shown) return;
    
    _shown = false;
    
    setVisible(false);
}

bool GameOverView::onTouchBegan(cocos2d::Touch*, cocos2d::Event*)
{
    hide();

    if (_onClose)
    {
        _onClose();
    }

    Director::getInstance()->getEventDispatcher()->removeEventListener(_touchListener);
    
    return false;
}

void GameOverView::onShow()
{
    _tapToRestart->stopAllActions();
    _tapToRestart->setVisible(true);
    _tapToRestart->setOpacity(0);
    
    auto fade_in = FadeIn::create(1.0f);
    
    _tapToRestart->runAction(fade_in);
    
    _score->run(1.0f);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_touchListener, 1);
}