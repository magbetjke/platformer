/**
 *
 * CommandBus.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include <queue>
#include <memory>

class CommandBus;

class Command : public std::enable_shared_from_this<Command>
{
public:
    friend class CommandBus;
    
    virtual ~Command(){}
    
    void setCompleteHandler(std::function<void(std::shared_ptr<Command>)> func)
    {
        _onComplete = func;
    }
protected:
    Command(){}
    
    virtual const char* getId() const = 0;
    
    void setCommandBus(CommandBus* commands)
    {
        _commands = commands;
    }
    
    virtual void execute()
    {
        notifyComplete();
    }
    
    void notifyComplete()
    {
        if (_onComplete)
            _onComplete(getptr());
    }
    
    std::shared_ptr<Command> getptr()
    {
        return shared_from_this();
    }

    CommandBus* _commands;
    
    std::function<void(std::shared_ptr<Command>)> _onComplete;
};

class CommandBus final
{
public:
    static CommandBus* getInstance();
    void addCommand(std::shared_ptr<Command> cmd);
private:
    static CommandBus* _instance;
    
    CommandBus();
    
    void onCompleteCommand(std::shared_ptr<Command> cmd);
    void runNextCommand();
    
    std::queue<std::shared_ptr<Command>> _commands;
    bool _busy;
};
