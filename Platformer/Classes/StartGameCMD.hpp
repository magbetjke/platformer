//
//  StartGameCMD.hpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#pragma once

#include "CommandBus.hpp"
#include "GameScene.hpp"

class StartGameCMD : public Command
{
public:
    static std::shared_ptr<StartGameCMD> create(Mechanic mechanic)
    {
        return std::make_shared<StartGameCMD>(mechanic);
    }
    
    virtual ~StartGameCMD(){}
    StartGameCMD(Mechanic mechanic):_mechanic(mechanic){}
    
protected:
    const char* getId() const override {return "StartGameCMD";}
    void execute() override;
    
    Mechanic _mechanic;
};
