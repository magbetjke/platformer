/**
 *
 * ShadowVolume.cpp
 *
 * @author a.eremin
 *
 */

#include "ShadowVolume.hpp"
#include "Box.hpp"

using namespace cocos2d;

const std::string ShadowVolume::SHADOW_SHADER = "shadowShader";

ShadowVolume::ShadowVolume()
{
    _renderCommand.func = CC_CALLBACK_0(ShadowVolume::onDraw, this);
}

GLProgram* ShadowVolume::getOrCreateShader()
{
    GLProgram* shader = GLProgramCache::getInstance()->getGLProgram(SHADOW_SHADER);
    
    if (!shader)
    {
        shader = GLProgram::createWithFilenames("shaders/shadow_volume.vert", "shaders/shadow_volume.frag");
    }
    
    return shader;
}

void ShadowVolume::onDraw()
{
    changeRenderState();
    drawMesh();
    restoreRenderState();
}

void ShadowVolume::changeRenderState()
{
    glGetIntegerv(GL_STENCIL_FUNC, (GLint *)&_currentStencilFunc);
    glGetIntegerv(GL_STENCIL_REF, &_currentStencilRef);
    glGetIntegerv(GL_STENCIL_VALUE_MASK, (GLint *)&_currentStencilValueMask);
    glGetIntegerv(GL_STENCIL_FAIL, (GLint *)&_currentStencilFail);
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_FAIL, (GLint *)&_currentStencilPassDepthFail);
    glGetIntegerv(GL_STENCIL_PASS_DEPTH_PASS, (GLint *)&_currentStencilPassDepthPass);

    _cullFaceEnabled = glIsEnabled(GL_CULL_FACE);
    glGetIntegerv(GL_CULL_FACE_MODE, &_cullFaceMode);
    _currentStencilEnabled = glIsEnabled(GL_STENCIL_TEST);
    glGetIntegerv(GL_STENCIL_WRITEMASK, (GLint *)&_currentStencilWriteMask);
    
    glGetBooleanv(GL_DEPTH_WRITEMASK, &_depthMask);
    
    _depthTestEnabled = glIsEnabled(GL_DEPTH_TEST);

    glEnable(GL_STENCIL_TEST);
    glStencilMask(0xFF);
    
    glClearStencil(0);
    glClear(GL_STENCIL_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_FALSE);

    glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
    
    glStencilFunc(GL_ALWAYS, 1, 0xFF);
    glStencilOpSeparate(GL_FRONT, GL_KEEP, GL_KEEP, GL_INCR);
    glStencilOpSeparate(GL_BACK, GL_KEEP, GL_KEEP, GL_DECR);
}

void ShadowVolume::drawMesh()
{
    auto mesh = this->getMesh();

    GL::bindTexture2D(0);
    GL::blendFunc(_blend.src, _blend.dst);

    glBindBuffer(GL_ARRAY_BUFFER, _mesh->getVertexBuffer());
    
    Color4F color(getDisplayedColor());
    color.a = getDisplayedOpacity() / 255.0f;

    _glProgramState->setUniformVec4("u_color", Vec4(color.r, color.g, color.b, color.a));

    _glProgramState->apply(this->getNodeToWorldTransform());

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh->getIndexBuffer());
    
    //draw to stencil
    glDrawElements(mesh->getPrimitiveType(), (GLsizei)mesh->getIndexCount(), mesh->getIndexFormat(), 0);
    
    CC_INCREMENT_GL_DRAWN_BATCHES_AND_VERTICES(1, mesh->getIndexCount());
    
    
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    
    glStencilFunc(GL_NOTEQUAL, 0, 0xFF);
//    glStencilFunc(GL_ALWAYS, 0, 0xFF); //DRAW VOLUME
    glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);
    
    glDepthMask(GL_TRUE);
    
    //draw result
    glDrawElements(mesh->getPrimitiveType(), (GLsizei)mesh->getIndexCount(), mesh->getIndexFormat(), 0);
    
    CC_INCREMENT_GL_DRAWN_BATCHES_AND_VERTICES(1, mesh->getIndexCount());
}

void ShadowVolume::restoreRenderState()
{
    _cullFaceEnabled ? glEnable(GL_CULL_FACE) : glDisable(GL_CULL_FACE);
    
    glCullFace(_cullFaceMode);
    
    _depthTestEnabled ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
    
    glStencilMask(_currentStencilWriteMask);
    _currentStencilEnabled ? glEnable(GL_STENCIL_TEST) : glDisable(GL_STENCIL_TEST);
    
    glStencilFunc(_currentStencilFunc, _currentStencilRef, _currentStencilValueMask);
    glStencilOp(_currentStencilFail, _currentStencilPassDepthFail, _currentStencilPassDepthPass);
    
    glDepthMask(_depthMask);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}



void ShadowVolume::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    auto lights = Director::getInstance()->getRunningScene()->getLights();
    
    DirectionLight* sun;
    for (auto light : lights)
    {
        if (light->getLightType() == LightType::DIRECTIONAL)
        {
            sun = dynamic_cast<DirectionLight*>(light);
            break;
        }
        
    }
    
    if (sun)
    {
        //todo cache, relocate
        auto to_light = sun->getDirection();
        Vec3 initial_angle {1.0f, 0.0f, 0.0f};
    
        this->setRotationQuat(rotationBetweenVectors(initial_angle, to_light));
    }

    _renderCommand.init(_globalZOrder);
    renderer->addCommand(&_renderCommand);
}

void ShadowVolume::buildMesh()
{
    std::vector<float> positions;
    std::vector<float> normals;
    std::vector<float> texs;
    Mesh::IndexArray indices;
    
    Vec3 vert;
    
    positions.push_back(0.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
    positions.push_back(0.0f);positions.push_back(5.0f);positions.push_back(5.0f);
    positions.push_back(10.0f);positions.push_back(5.0f);positions.push_back(5.0f);
    positions.push_back(10.0f);positions.push_back(-5.0f);positions.push_back(5.0f);
    
    positions.push_back(0.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
    positions.push_back(0.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
    positions.push_back(10.0f);positions.push_back(5.0f);positions.push_back(-5.0f);
    positions.push_back(10.0f);positions.push_back(-5.0f);positions.push_back(-5.0f);
    
    for (int i = 0; i != 8; i++)
    {
         normals.push_back(0.0f);normals.push_back(0.0f);normals.push_back(1.0f);
    }
    
    //front
    indices.push_back(2);
    indices.push_back(1);
    indices.push_back(0);
    
    indices.push_back(3);
    indices.push_back(2);
    indices.push_back(0);
    
    //top
    indices.push_back(6);
    indices.push_back(5);
    indices.push_back(1);
    
    indices.push_back(2);
    indices.push_back(6);
    indices.push_back(1);
    
    //back
    indices.push_back(5);
    indices.push_back(6);
    indices.push_back(7);
    
    indices.push_back(4);
    indices.push_back(5);
    indices.push_back(7);
    
    //bottom
    indices.push_back(4);
    indices.push_back(7);
    indices.push_back(0);
    
    indices.push_back(7);
    indices.push_back(3);
    indices.push_back(0);
    
//    //left
//    indices.push_back(1);
//    indices.push_back(4);
//    indices.push_back(0);
//    
//    indices.push_back(1);
//    indices.push_back(5);
//    indices.push_back(4);
    
    //right
    indices.push_back(7);
    indices.push_back(2);
    indices.push_back(3);
    
    indices.push_back(7);
    indices.push_back(6);
    indices.push_back(2);
    
    _mesh = Mesh::create(positions, normals, texs, indices);
}

Quaternion ShadowVolume::rotationBetweenVectors(Vec3 start, Vec3 dest)
{
    start.normalize();
    dest.normalize();
 
    float cosTheta = Vec3::dot(start, dest);
    Vec3 rotationAxis;
 
    if (cosTheta < -1 + 0.001f){
        // special case when vectors in opposite directions:
        // there is no "ideal" rotation axis
        // So guess one; any will do as long as it's perpendicular to start
        Vec3::cross(Vec3(0.0f, 0.0f, 1.0f), start, &rotationAxis);
        
        if (rotationAxis.lengthSquared() < 0.01 ) // bad luck, they were parallel, try again!
            Vec3::cross(Vec3(1.0f, 0.0f, 0.0f), start, &rotationAxis);
 
        rotationAxis.normalize();
        Quaternion result;
        Quaternion::createFromAxisAngle(rotationAxis, CC_DEGREES_TO_RADIANS(180.0f), &result);
        return result;
    }
 
    Vec3::cross(start, dest, &rotationAxis);
 
    float s = sqrt( (1+cosTheta)*2 );
    float invs = 1 / s;
 
    return Quaternion(
        rotationAxis.x * invs,
        rotationAxis.y * invs,
        rotationAxis.z * invs,
        s * 0.5f
    );
}

