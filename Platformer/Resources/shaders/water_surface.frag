#ifdef GL_ES
varying mediump vec2 v_texture_coord;
#else
varying vec2 v_texture_coord;
#endif
uniform vec3 u_DirLightSourceColor[1];
uniform vec3 u_DirLightSourceDirection[1];

uniform vec3 u_AmbientLightSourceColor;

uniform vec4 u_color;
varying vec3 v_normal;

#define PARALLAX_WATER 0
#define DISTORT_WATER 0

#define WATER_COL vec3(0.0, 0.4453, 0.7305)
#define WATER2_COL vec3(0.0, 0.4180, 0.6758)
#define FOAM_COL vec3(0.8125, 0.9609, 0.9648)
#define FOG_COL vec3(0.6406, 0.9453, 0.9336)
#define SKY_COL vec3(0.0, 0.8203, 1.0)

#define M_2PI 6.283185307
#define M_6PI 18.84955592

vec3 computeLighting(vec3 normalVector, vec3 lightDirection, vec3 lightColor, float attenuation)
{
    float diffuse = max(dot(normalVector, lightDirection), 0.3);
    vec3 diffuseColor = lightColor  * diffuse * attenuation;
    
    return diffuseColor;
}

void main(void)
{
//    vec3 normal  = normalize(v_normal);
//    vec4 combinedColor = vec4(u_AmbientLightSourceColor, 1.0);
//
//    vec3 lightDirection = normalize(u_DirLightSourceDirection[0] * 2.0);
//    
//    combinedColor.xyz += computeLighting(normal, -lightDirection, u_DirLightSourceColor[0], 1.0);
    
    float dist = gl_FragCoord.w * 700.0; //todo
//    gl_FragColor = vec4(u_color.xyz * combinedColor.xyz , dist);
    
    vec3 color = texture2D(CC_Texture0, v_texture_coord).rgb * u_color.rgb;
//    gl_FragColor = texture2D(CC_Texture0, v_texture_coord) * u_color;
    gl_FragColor.rgb = mix(FOG_COL, color, min(dist, 1.0));
    gl_FragColor.a = 1.0;
}

