//
//  GameObjects.h
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#pragma once

#include <list>

template<class T, int LIMIT = 10>
class GameObjects final
{
public:
    GameObjects()
    {
        prealocate();
    }
    
    T* getAvailable()
    {
        if (_container.size() < LIMIT)
        {
            auto obj = T::create();
            _container.push_back(obj);
            return obj;
        }
    
        T* obj = _container.front();
        _container.push_back(obj);
        _container.pop_front();
        return obj;
    }
    
    void clear()
    {
        _container.clear();
    }
    
    typename std::list<T*>::iterator begin()
    {
        return _container.begin();
    }
    
    typename std::list<T*>::iterator end()
    {
        return _container.end();
    }
    
    typename std::list<T*>::const_iterator cbegin()
    {
        return _container.cbegin();
    }
    
    typename std::list<T*>::const_iterator cend()
    {
        return _container.cend();
    }
    
private:
    void prealocate()
    {
        for (size_t i = 0; i != LIMIT; i++)
        {
            getAvailable();
        }
    }

    std::list<T*> _container;
};
