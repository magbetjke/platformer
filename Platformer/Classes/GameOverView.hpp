//
//  GameOverView.hpp
//  Platformer
//
//  Created by Алексей Еремин on 04/05/15.
//
//

#pragma once

#include "cocos2d.h"
#include "cocosGUI.h"

class AnimatedDigits;

class GameOverView : public cocos2d::ui::Layout
{
public:
    CREATE_FUNC(GameOverView)
    
    GameOverView();
    virtual ~GameOverView();
    
    void show(int score, int coins);
    void hide();
    
    bool isShown() const {return _shown;}
    
    void setOnClose(std::function<void()> value)
    {
        _onClose = value;
    }
protected:
    void initRenderer() override;
    
private:
    bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
    void onShow();

    std::function<void()> _onClose;

    AnimatedDigits* _score;
    cocos2d::ui::Text* _coins;
    cocos2d::ui::Text* _tapToRestart;
    
    cocos2d::EventListenerTouchOneByOne* _touchListener;
    
    bool _shown;
    
    float _scoreAnimationTime;
};