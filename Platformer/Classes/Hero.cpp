//
//  Hero.cpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#include "Hero.hpp"

using namespace cocos2d;

Hero::Hero()
    :_state(State::ON_SURFACE)
    ,_ascensionInitialSpeed(400.0f)
    ,_ascensionAcceleration(-10.0f)
    ,_fallingAcceleration(1700.0f)
    ,_minJumpHeightInUnits(1)
    ,_maxJumpHeightInUnits(3)
    ,_minJumpHeight(0.0f)
    ,_maxJumpHeight(0.0f)
{
}

Hero::~Hero()
{
}

void Hero::setState(State value)
{
    _state = value;
}

void Hero::setVolume(const cocos2d::Vec3& value)
{
    _minJumpHeight = value.y * _minJumpHeightInUnits;
    _maxJumpHeight = value.y * _maxJumpHeightInUnits;

//    _shadowVolume->setVolume(cocos2d::Vec3(value.x * 10.0f, value.y * 0.9, value.z * 0.7));
    
    _shadowPenumbra->setVolume(cocos2d::Vec3(value.x * 10.0f, value.y * 1.1, value.z * 0.95));

    Box::setVolume(value);
}

bool Hero::init()
{
    if (Box::init())
    {
        this->setCascadeOpacityEnabled(false);
        this->setCascadeColorEnabled(false);
        
//        _shadowVolume = ShadowVolume::create();
//        _shadowVolume->setColor(cocos2d::Color3B::BLACK);
//        _shadowVolume->setOpacity(45);
//        addChild(_shadowVolume);
        
         _shadowPenumbra = ShadowVolume::create();
        _shadowPenumbra->setColor(cocos2d::Color3B::BLACK);
        _shadowPenumbra->setOpacity(45);
        addChild(_shadowPenumbra);
        
        _programState->setUniformInt("u_materialId", 1);
    
        return true;
    }
    
    return false;
}

void Hero::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{    
    Box::draw(renderer, transform, flags);
}

//Quaternion Hero::rotationBetweenVectors(Vec3 start, Vec3 dest){
//    start.normalize();
//    dest.normalize();
// 
//    float cosTheta = Vec3::dot(start, dest);
//    Vec3 rotationAxis;
// 
//    if (cosTheta < -1 + 0.001f){
//        // special case when vectors in opposite directions:
//        // there is no "ideal" rotation axis
//        // So guess one; any will do as long as it's perpendicular to start
//        Vec3::cross(Vec3(0.0f, 0.0f, 1.0f), start, &rotationAxis);
//        
//        if (rotationAxis.lengthSquared() < 0.01 ) // bad luck, they were parallel, try again!
//            Vec3::cross(Vec3(1.0f, 0.0f, 0.0f), start, &rotationAxis);
// 
//        rotationAxis.normalize();
//        Quaternion result;
//        Quaternion::createFromAxisAngle(rotationAxis, CC_DEGREES_TO_RADIANS(180.0f), &result);
//        return result;
//    }
// 
//    Vec3::cross(start, dest, &rotationAxis);
// 
//    float s = sqrt( (1+cosTheta)*2 );
//    float invs = 1 / s;
// 
//    return Quaternion(
//        rotationAxis.x * invs,
//        rotationAxis.y * invs,
//        rotationAxis.z * invs,
//        s * 0.5f
//    );
//}