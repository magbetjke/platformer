//
//  GoToStartScreenCMD.hpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#pragma once

#include "CommandBus.hpp"

class GoToStartScreenCMD : public Command
{
public:

    static std::shared_ptr<GoToStartScreenCMD> create()
    {
        return std::make_shared<GoToStartScreenCMD>();
    }
    
    virtual ~GoToStartScreenCMD() {}
    GoToStartScreenCMD() {}
    
protected:
    const char* getId() const override {return "GoToStartScreenCMD";}
    void execute() override;
};