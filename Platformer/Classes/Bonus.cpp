/**
 *
 * Bonus.cpp
 *
 * @author a.eremin
 *
 */

#include "Bonus.hpp"

using namespace cocos2d;

Bonus::Bonus()
    :_shadow(nullptr)
{
}

Bonus::~Bonus()
{
    CC_SAFE_RELEASE(_shadow);
}

bool Bonus::init()
{
    if (Box::init())
    {
        _shadow = PlanarShadow::create();
        _shadow->setColor(Color3B::BLACK);
        _shadow->setOpacity(25);
        _shadow->retain();
        _shadow->addOccluder(this);
        _shadow->setGlobalZOrder(_globalZOrder);
        
        _programState->setUniformInt("u_materialId", 1);
        
        return true;
    }
    
    return false;
}

void Bonus::draw(Renderer *renderer, const Mat4& transform, uint32_t flags)
{
    _shadow->draw(renderer, transform, flags);

    Box::draw(renderer, transform, flags);
}