/**
 *
 * Box.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include "cocos2d.h"
#include "PlanarShadow.hpp"

class Box : public cocos2d::Sprite3D
{
public:
    CREATE_FUNC(Box)
    
    static const std::string BOX_SHADER;
    
    static Box* create(const cocos2d::Vec3& value);

    virtual void setVolume(const cocos2d::Vec3& value);
    cocos2d::Vec3 getVolume() const {return _volumeSize;}
    
    float getRadiusXY() const {return _radius_xy;}
    
    Box();
    virtual ~Box();
protected:
    bool init() override;

    virtual void visit(cocos2d::Renderer *renderer, const cocos2d::Mat4& parentTransform, uint32_t parentFlags) override;
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    
    virtual cocos2d::GLProgram* getOrCreateShader();
    
    virtual void buildMesh();
    
    cocos2d::Mesh* _mesh;
    cocos2d::GLProgramState* _programState;
    
    cocos2d::Vec3 _volumeSize;
private:
    
    void applyShader();
    void updateVolume();
    

    float _radius_xy;
    bool _sizeIsDirty;
};