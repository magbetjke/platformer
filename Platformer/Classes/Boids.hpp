//
//  Boids.hpp
//  Platformer
//
//  Created by Алексей Еремин on 19/01/16.
//
//

#ifndef Boids_hpp
#define Boids_hpp

#include <stdio.h>
#include "cocos2d.h"

class Boid
{
public:
    enum class EdgeBehav {
        kEdgeNone
        , kEdgeBounce
        , kEdgeWrap
    };
    
    void setPosition(const cocos2d::Vec3& pos)
    {
        m_position = pos;
    }
    cocos2d::Vec3 getPosition() { return m_position; }
    
    float getMaxForce() { return m_maxForce; }
    void setMaxForce(float value)
    {
        if (m_maxForce != value) {
            m_maxForce = value;
            m_maxForceSQ = value * value;
        }
    }
    
    float getMaxSpeed() { return m_maxSpeed; }
    void setMaxSpeed(float value)
    {
        if (m_maxSpeed != value) {
            m_maxSpeed = value;
            m_maxSpeedSQ = value * value;
        }
    }
    
    EdgeBehav getEdgeBehaviour() { return m_edgeBehavior; }
    void setEdgeBehaviour(EdgeBehav value)
    {
        m_edgeBehavior = value;
    }
    
    cocos2d::Vec3 getBoundsCenter() { return m_boundsCenter; }
    
    void setBoundsCenter(const cocos2d::Vec3& value)
    {
        m_boundsCenter = value;
    }
    
    float getBoundRadius() { return m_boundsRadius; }
    void setBoundRadius(float value)
    {
        m_boundsRadius = value;
    }
    
    float getWanderStep() { return m_wanderStep; }
    void setWanderStep(float value)
    {
        m_wanderStep = value;
    }
    
    float getWanderDistance() { return m_wanderDistance; }
    void setWanderDistance(float value)
    {
        m_wanderDistance = value;
    }
    
    float getWanderRadius() { return m_wanderRadius; }
    void setWanderRadius(float value)
    {
        m_wanderRadius = value;
    }
    
    cocos2d::Vec3 getVelocity() { return m_velocity; }
    
    void flock(const std::vector<Boid>& boids, float sepW = 0.5f, float algnW = 0.5f, float cohW = 0.2f, float sepD = 100.0f, float algnD = 200.0f, float cohD = 200.0f);
private:
    void separate(const std::vector<Boid>& boids, float distance, float weight);
    void align(const std::vector<Boid>& boids, float distance, float weight);
    void cohesion(const std::vector<Boid>& boids, float distance, float weight);
    
    cocos2d::Vec3 getSeparation(const std::vector<Boid>& boids, float separation = 25.0f);
    
    cocos2d::Vec3 getAlignment(const std::vector<Boid>& boids, float neighborDistance = 50.0f);
    cocos2d::Vec3 getCohesion(const std::vector<Boid>& boids, float neighborDistance = 50.0f);
    
    cocos2d::Vec3 steer(const cocos2d::Vec3& target, bool ease = false, float easeDistance = 100.0f);
    
    void constraintToRect(const cocos2d::Rect& rect, EdgeBehav behavior, float zMin, float zMax);
    
    void brake(float brakingForce = 0.1f);
    void seek(const cocos2d::Vec3& target, float multiplier = 1.0f);
    void arrive(const cocos2d::Vec3& target, float easeDistance = 100.0f, float multiplier = 1.0f);
    void flee(const cocos2d::Vec3& target, float panicDistance = 100.0f, float multiplier = 1.0f);
    void wander(float multiplier = 1.0f);
    
    void update();
    
    cocos2d::Vec3 m_oldPosition;
    cocos2d::Vec3 m_position;
    cocos2d::Vec3 m_steeringForce;
    cocos2d::Vec3 m_acceleration;
    cocos2d::Vec3 m_velocity;
    cocos2d::Vec3 m_boundsCenter;
    EdgeBehav m_edgeBehavior;
    float m_boundsRadius;
    
    
    float m_radius;
    float m_distance;
    float m_maxSpeedSQ;
    float m_maxSpeed;
    
    float m_maxForce;
    float m_maxForceSQ;
    
    float m_wanderTheta;
    float m_wanderPhi;
    float m_wanderPsi;
    float m_wanderDistance;
    float m_wanderStep;
    float m_wanderRadius;
    
};

#endif /* Boids_hpp */
