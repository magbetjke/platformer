//
//  BlockGenerator.cpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//
#include <random>

#include "BlockGenerator.hpp"

using namespace cocos2d;

BlockGenerator::BlockGenerator()
    :_step(100.0f)
    ,_maxEmptyLength(2)
    ,_minEmptyLength(1)
    ,_maxSolidLength(2)
    ,_minSolidLength(1)
    ,_blockHeight(50.0f)
    ,_blockDepth(20.0f)
{
}

BlockGenerator::~BlockGenerator()
{
}

BlockDescriptor BlockGenerator::getNextBlock(const BlockDescriptor& current)
{
//    return {
//            BlockDescriptor::Type::SOLID,
//            getVolume(_blockDepth, _blockHeight, _step, _minSolidLength, _maxSolidLength)
//            };

    if (current.type == BlockDescriptor::Type::EMPTY)
    {
        return {
            BlockDescriptor::Type::SOLID,
            getVolume(_blockDepth, _blockHeight, _step, _minSolidLength, _maxSolidLength),
            true
            };
    }
    else
    {
        return {
            BlockDescriptor::Type::EMPTY,
            getVolume(_blockDepth, _blockHeight, _step, _minEmptyLength, _maxEmptyLength)
            };
    }
}

Vec3 BlockGenerator::getVolume(float depth, float height, float step, int minLength, int maxLength)
{
    std::random_device rd;
    
    std::default_random_engine engine(rd());
    std::uniform_int_distribution<int> distr(minLength, maxLength);
    
    auto lenght = distr(engine);
    
    return Vec3(lenght * step, height, depth);
}