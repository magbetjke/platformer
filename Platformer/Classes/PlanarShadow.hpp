/**
 *
 * PlanarShadow.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include "cocos2d.h"
class Bonus;
class Box;

class PlanarShadow : public cocos2d::Sprite3D
{
public:
    friend class Bonus;

    CREATE_FUNC(PlanarShadow)
    
    PlanarShadow();
    virtual ~PlanarShadow();
    
    void addOccluder(cocos2d::Sprite3D* occluder);
    void setReceiver(Box* receiver);
    void removeAllOccluders();
    
protected:
    static const std::string PLANAR_SHADOW_SHADER;
    
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    
    void removeOccluder(cocos2d::Sprite3D* occluder);

//    cocos2d::MeshCommand _command;
    std::map<cocos2d::Sprite3D*, cocos2d::MeshCommand> _renderCommands;
    Box* _receiver;
};