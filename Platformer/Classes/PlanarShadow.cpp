/**
 *
 * PlanarShadow.cpp
 *
 * @author a.eremin
 *
 */

#include "PlanarShadow.hpp"
#include "Box.hpp"

//todo not finished

using namespace cocos2d;

const std::string PlanarShadow::PLANAR_SHADOW_SHADER = "planarShadowShader";

PlanarShadow::PlanarShadow()
    :_receiver(nullptr)
{
    _shaderUsingLight = true;
    
    GLProgram* shader = GLProgramCache::getInstance()->getGLProgram(PLANAR_SHADOW_SHADER);
    
    if (!shader)
    {
        shader = GLProgram::createWithFilenames("shaders/box_planar_shadow.vert", "shaders/box_planar_shadow.frag");
    }

    setGLProgramState(GLProgramState::create(shader));
}

PlanarShadow::~PlanarShadow()
{
//    for (auto mesh : _occluders)
//    {
//        CC_SAFE_RELEASE(mesh);
//    }
}

void PlanarShadow::addOccluder(cocos2d::Sprite3D *occluder)
{
    auto clone = occluder->getMesh();

//    _renderCommands[occluder] = MeshCommand();

    MeshCommand cmd;
    cmd.setSkipBatching(true);
    _renderCommands.insert(std::pair<Sprite3D*, MeshCommand>(occluder, cmd));
    
    auto attributeCount = clone->getMeshVertexAttribCount();
    
    long offset = 0;
    for (auto i = 0; i < attributeCount; i++) {
        auto meshattribute = clone->getMeshVertexAttribute(i);
        _glProgramState->setVertexAttribPointer(s_attributeNames[meshattribute.vertexAttrib],
            meshattribute.size,
            meshattribute.type,
            GL_FALSE,
            clone->getVertexSizeInBytes(),
            (GLvoid*)offset);
        offset += meshattribute.attribSizeBytes;
    }
}

void PlanarShadow::setReceiver(Box* receiver)
{
    _receiver = receiver;
}

void PlanarShadow::removeAllOccluders()
{
    _renderCommands.clear();
}

void PlanarShadow::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    if (!_receiver || !_receiver->isVisible()) return;
    
    //TODO
    auto mv = _receiver->getWorldToNodeTransform();
    Vec3 ts;
    mv.getTranslation(&ts);
    
    _glProgramState->setUniformFloat("u_receiver_y", _receiver->getPosition3D().y + _receiver->getVolume().y * 2 + 2 - ts.y);
    
    //TODO
    
    Color4F color(getDisplayedColor());
    color.a = getDisplayedOpacity() / 255.0f;
    
    for (auto item : _renderCommands)
    {
        auto occluder = item.first;
        auto mesh = occluder->getMesh();
        
        auto& command = _renderCommands[occluder];
        
        command.init(_globalZOrder, 0, _glProgramState, _blend, mesh->getVertexBuffer(), mesh->getIndexBuffer(), mesh->getPrimitiveType(), mesh->getIndexFormat(), mesh->getIndexCount(), occluder->getNodeToWorldTransform(), flags);
        
        command.setDisplayColor(Vec4(color.r, color.g, color.b, color.a));
//        command.setDepthTestEnabled(true);
        
        renderer->addCommand(&command);
    }

    Sprite3D::draw(renderer, transform, flags);
}

void PlanarShadow::removeOccluder(cocos2d::Sprite3D* occluder)
{
    auto index = _renderCommands.find(occluder);
    if (index != _renderCommands.end())
    {
        _renderCommands.erase(index);
    }
}