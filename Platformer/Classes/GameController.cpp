//
//  GameController.cpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#include "GameController.hpp"

#include "Box.hpp"
#include "GameScene.hpp"
#include "GoToStartScreenCMD.hpp"
#include "Water.hpp"
#include "Bonus.hpp"

#define DEFAULT_WATER_UV_SPEED 100.0f

using namespace cocos2d;

void GameController::setScene(GameScene *value)
{
    CCASSERT(value != nullptr && _scene == nullptr, "One must not assign null or reassign a scene value");
    
    _scene = value;
    
    _hero = _scene->getHero();
    _world = _scene->getWorld();
    _water = _scene->getWater();
    _sky = _scene->getSky();
    
    _scene->getGameOverView()->setOnClose(CC_CALLBACK_0(GameController::initForTest, this));
    
//    _scene->getGameOverView()->setOnClose([](){CommandBus::getInstance()->addCommand(GoToStartScreenCMD::create());});
    
    for (auto obj : _groundObjs)
    {
        _scene->addObjectToWorld(obj);
    }
    
    for (auto obj : _powerupObjs)
    {
        obj->setVolume(Vec3(20, 20, 20));
        obj->setColor(Color3B::YELLOW);
        _scene->addObjectToWorld(obj);
    }
}


GameController::GameController()
    :_scene(nullptr)
    ,_pendingToStart(false)
    ,_running(false)
    ,_heroVerticalVelocity(0.0f)
    ,_currenSpawnPoint(0.0f)
    ,_heroLandedY(0.0f)
    ,_touched(false)
    ,_groundObjs()
    ,_powerupObjs()
    ,_firstLaunch(true)
    ,_currentAltitude(0.0f)
    ,_altitudeAscendanceSpeed(0.0f)
{
    _generator.setStep(150);
    _generator.setBlockDepth(PLATFORM_DEPTH);
    _generator.setBlockHeight(PLATFORM_HEIGHT);
    _generator.setMaxEmptyLength(2);
    _generator.setMinEmptyLength(1);
    _generator.setMaxSolidLength(4);
    _generator.setMinSolidLength(2);
    
    _touchListener = EventListenerTouchOneByOne::create();
    _touchListener->onTouchBegan = CC_CALLBACK_2(GameController::onTouchBegan, this);
    _touchListener->onTouchEnded = CC_CALLBACK_2(GameController::onTouchEnded, this);
    
    Director::getInstance()->getEventDispatcher()->addEventListenerWithFixedPriority(_touchListener, 1);
}

GameController::~GameController()
{
    for (auto obj : _groundObjs)
    {
        _scene->removeChild(obj);
    }
    _groundObjs.clear();
    
    for (auto obj : _powerupObjs)
    {
        _scene->removeChild(obj);
    }
    _powerupObjs.clear();
    
    Director::getInstance()->getEventDispatcher()->removeEventListener(_touchListener);
    stop();
}

void GameController::initForTest()
{
    init(_gameConfig);
}

void GameController::init(GameConfig config)
{
    CCASSERT(_scene != nullptr, "Set _scene first");
    
    _gameConfig = config;

    stop();
    reset();
    
    auto size = Vec3(450, 200, 100);
    auto start_block = BlockDescriptor{BlockDescriptor::Type::SOLID, size, false};

    auto block = addBlock(start_block, false);
    
    _heroLandedY = block->getPositionY() + (size.y + _hero->getVolume().y) * 0.5f;
    _hero->setPosition3D(Vec3(size.x * 0.2f, _heroLandedY, 0.0f));
    
    for (int i = 1; i != 3; i++)
    {
        addBlock(_generator.getNextBlock(_currentBlock), false);
    }
    
    if (_firstLaunch)
    {
        _scene->showHint();
        _pendingToStart = true;
        
        _firstLaunch = false;
    }
    else
    {
        start();
    }
}

void GameController::reset()
{
    _model.reset();
    
    _world->setPositionX(0.0f);
    
    _heroVerticalVelocity = 0.0f;
    _hero->setState(Hero::State::ON_SURFACE);
    
    _heroDrawn = false;
    _speed = _gameConfig.speed;
    _currentAltitude = 0.0f;
    _water->setUVSpeed(DEFAULT_WATER_UV_SPEED);
    _sky->setUVSpeed(0.0f);
    _currentSpeed = 0.0f;
    _spawnDistance = _gameConfig.spawnDistance;
    _gainScoreSpeed = _gameConfig.gainScoreSpeed;
    _altitudeAscendanceSpeed = _gameConfig.altitudeAscendanceSpeed;
    
    float far_away_x = -10000;
    for (auto obj : _groundObjs)
    {
        obj->setPositionX(far_away_x);
    }
    for (auto obj : _powerupObjs)
    {
        obj->setPositionX(far_away_x);
    }

    _currenSpawnPoint = 0.0f;
    _touched = false;
}

void GameController::start()
{
    _pendingToStart = false;
    
    _scene->hideHint();
    
    _running = true;
    
    Director::getInstance()->getScheduler()->scheduleUpdate(this, 0, false);
}

void GameController::stop()
{
    if (_running)
    {
        _running = false;
        
        Director::getInstance()->getScheduler()->unscheduleUpdate(this);
    }
}

void GameController::showStats()
{
    if (!_scene->getGameOverView()->isShown())
    {
        _scene->getGameOverView()->show(_model.score, _model.coins);
    }
    
}

bool GameController::onTouchBegan(cocos2d::Touch*, cocos2d::Event*)
{
    if (_pendingToStart)
    {
        start();
        return false;
    }

    if (!_running || _heroDrawn) return false;

    if (_hero->isOnSurface())
    {
        
        _touched = true;
        _hero->setState(Hero::State::JUMP_UP);
        _heroVerticalVelocity = _hero->getAscensionInitialSpeed();
    }
    
    return true;
}

void GameController::onTouchEnded(cocos2d::Touch*, cocos2d::Event*)
{
    if (!_running) return;

    if (_hero->isJumpingUP())
    {
        _touched = false;
    }
}

void GameController::update(float delta)
{
    if (_running)
    {
        updateHero(delta);
        updateWorld(delta);
        updateScore(delta);
    }
}

void GameController::updateWorld(float delta)
{
    _world->setPositionX(_world->getPositionX() - delta * _currentSpeed);
    
    _water->setUVSpeed(DEFAULT_WATER_UV_SPEED + _currentSpeed);
    _sky->setUVSpeed(_currentSpeed * 0.005f);
    
    if (_hero->isFalling() && _speed != 0.0f) //stop world movement
    {
        _speed = 0.0f;
        _altitudeAscendanceSpeed = 0.0f;
        
        showStats();
    }
    
    _currentAltitude -= _altitudeAscendanceSpeed * delta;
    if (_currentAltitude < _gameConfig.minAltitude)
    {
        _currentAltitude = _gameConfig.minAltitude;
    }
    
    _speed += _gameConfig.speedAcceleration * delta;
    
    if (_currentSpeed != _speed)
    {
        if (_currentSpeed > _speed)
        {
            _currentSpeed -= (_currentSpeed - _speed) * 0.07;
            if (_currentSpeed < _speed)
            {
                _currentSpeed = _speed;
            }
        }
        else //_currentSpeed < _speed
        {
            _currentSpeed += (_speed - _currentSpeed) * 0.05;
            if (_currentSpeed > _speed)
            {
                _currentSpeed = _speed;
            }
        }
    }
            
    if (_currenSpawnPoint - _hero->getPositionX() < _spawnDistance)
    {
        addBlock(_generator.getNextBlock(_currentBlock));
    }
}

void GameController::updateHero(float delta)
{
    auto pos_y = _hero->getPositionY() + _heroVerticalVelocity * delta;
    _hero->setPositionY(pos_y);
    
    if (pos_y <= HERO_DEATH_THRESHOLD)
    {
        _heroDrawn = true;
        showStats();
    }

    //checkStates
    if (!_hero->isFalling() || !checkCollisionAgainstWalls())
    {
        _hero->setPositionX(_hero->getPositionX() + _currentSpeed * delta);
    }
    
    if (_hero->isOnSurface())
    {
        checkCollisionAgainstSurface();
        checkCollisionAgainstPowerups();
    }
    else if (_hero->isJumpingDown())
    {
        checkCollisionAgainstSurface();
        checkCollisionAgainstPowerups();
    }
    else if (_hero->isJumpingUP())
    {
        checkHeroJump();
        checkCollisionAgainstPowerups();
    }
    
    //change velocity
    if (_hero->isFalling() || _hero->isJumpingDown())
    {
        _heroVerticalVelocity -= _hero->getFallingAcceleration() * delta;

    }
    else if (_hero->isJumpingUP())
    {
        _heroVerticalVelocity += _hero->getAscensionAcceleration() * delta;

    }
    else
    {
        _heroVerticalVelocity = 0; //onSurface

    }
}

void GameController::updateScore(float delta)
{
    if (!_hero->isFalling())
    {
        _model.score += delta * _gainScoreSpeed;
    }
    
    auto bottom = HERO_DEATH_THRESHOLD - (PLATFORM_HEIGHT + _hero->getVolume().y) * 0.5;
    auto total = _gameConfig.maxAltitude - bottom;
    auto curr = _currentAltitude - bottom;
    float to_ui = curr * 100 / total;
    _scene->setUIAltitude(to_ui);
    
    _scene->setUISpeed(_currentSpeed);
}

void GameController::checkHeroJump()
{
    CCASSERT(_hero->isJumpingUP(), "Wrong usage");

    auto hero_y = _hero->getPositionY();
    
    auto lower_limit = _hero->getMinJumpHeight() + _heroLandedY;
    auto upper_limit = _hero->getMaxJumpHeight() + _heroLandedY;
    
    if (hero_y > upper_limit || (!_touched && hero_y > lower_limit))
    {
        _hero->setState(Hero::State::JUMP_DOWN); //break jump
    }
}

bool GameController::checkCollisionAgainstWalls()
{
    CCASSERT(_hero->isFalling(), "Wrong usage");

    auto hero_right = _hero->getPositionX() + _hero->getVolume().x * 0.5f;

    for (auto obj: _groundObjs) {
        auto obj_left = obj->getPositionX() - obj->getVolume().x * 0.5f;
        auto obj_right = obj->getPositionX() + obj->getVolume().x * 0.5f;
        if (hero_right < obj_right && hero_right > obj_left)
        {
            _hero->setPositionX(obj_left - _hero->getVolume().x * 0.5f);
            return true;
        }
    }
    
    return false;
}

void GameController::checkCollisionAgainstSurface()
{
    CCASSERT(_hero->isJumpingDown() || _hero->isOnSurface(), "Wrong usage");
    
    auto hero_x = _hero->getPositionX();
    auto hero_bottom_y = _hero->getPositionY() - _hero->getVolume().y * 0.5f;
    
    for (auto obj: _groundObjs) {
        auto obj_x = obj->getPositionX();
        auto obj_half_length = obj->getVolume().x * 0.5f;
        auto left = obj_x - obj_half_length;
        auto right = obj_x + obj_half_length;
    
        if (hero_x >= left && hero_x <= right) //block under the hero
        {
            if (_hero->getAABB().intersects(obj->getAABB()))
            {
                _heroLandedY = obj->getPositionY() + (obj->getVolume().y + _hero->getVolume().y) * 0.5f;
                _hero->setPositionY(_heroLandedY);
                
                _hero->setState(Hero::State::ON_SURFACE);
                return;
                
            }
//            
//            if (_hero->isOnSurface())
//            {
//                return;
//            }
            
            return;
        }
        
        if (hero_x + _hero->getVolume().x * 0.5f < left) //next block
        {
            if (_hero->isOnSurface() || hero_bottom_y <= obj->getPositionY() + obj->getVolume().y * 0.5f)
            {
                _hero->setState(Hero::State::FALL);
            }
            
            return;
        }
    }
}

void GameController::checkCollisionAgainstPowerups()
{
    if (!_hero->isJumpingUP() && !_hero->isJumpingDown()) return;

    Vec2 hero_xy {_hero->getPositionX(), _hero->getPositionY()};
    auto hero_radii = _hero->getRadiusXY();
    for (auto powerup : _powerupObjs)
    {
        Vec2 powerup_xy {powerup->getPositionX(), powerup->getPositionY()};
        auto distance = hero_xy.distance(powerup_xy);
        if (hero_radii + powerup->getRadiusXY() >= distance)
        {
            collectPowerUp(powerup);
        }
    }
}

void GameController::collectPowerUp(Box* powerup)
{
    powerup->setPositionX(-10000);
    
    _model.coins++;
    
    _currentAltitude += _gameConfig.altitudeBonus;
    
    if (_currentAltitude > _gameConfig.maxAltitude)
    {
        _currentAltitude = _gameConfig.maxAltitude;
    }
    
    _speed -= SPEED_BONUS;
    if (_speed < _gameConfig.minSpeed)
    {
        _speed = _gameConfig.minSpeed;
    }
}

Box* GameController::addBlock(const BlockDescriptor& descr, bool animated)
{
    _currentBlock = descr;
    
    auto volume_size = descr.size;
    
    Box* box;
    
    if (descr.type == BlockDescriptor::Type::SOLID)
    {
        box = _groundObjs.getAvailable();
        box->stopAllActions();
        box->setVolume(volume_size);
        box->setColor(Color3B(120, 200, 225));
        
        
//        float vertical_pos = - 50.0f;
        float vertical_pos = _currentAltitude;
        const float show_up_duration = 1.3f;
        
        auto pos_x = volume_size.x * 0.5f + _currenSpawnPoint;
        box->setPosition3D(Vec3(pos_x, vertical_pos, 0.0f));
        
        if (animated)
        {
            box->setPositionY(-4 * volume_size.y);
            
            auto move = MoveBy::create(show_up_duration, Vec3(0.0f, vertical_pos - box->getPositionY(), 0.0f));
            box->runAction(EaseExponentialOut::create(move));
        }
        
        if (descr.containsPowerUp)
        {
            float offset = volume_size.y * 0.4f;
            auto bonus_y = vertical_pos + volume_size.y * 0.5 + offset;
            auto powerup = _powerupObjs.getAvailable();
            powerup->setPosition3D(Vec3(pos_x, bonus_y, 0.0f));
            powerup->stopAllActions();
            powerup->getShadow()->setReceiver(box);
            powerup->runAction(RepeatForever::create(RotateBy::create(0.5f, Vec3(30.0f, 0.0f, 30.0f))));
            
            if (animated)
            {
                auto start_y = box->getPositionY() + volume_size.y * 0.5 + offset;
                powerup->setPositionY(start_y);
                auto move = MoveBy::create(show_up_duration, Vec3(0.0f, bonus_y - start_y, 0.0f));
                powerup->runAction(EaseExponentialOut::create(move));
            }
        }
    }
    
    _currenSpawnPoint += volume_size.x;
    
    return box;
}