/**
 *
 * Bonus.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include "Box.hpp"
#include "ShadowVolume.hpp"
#include "PlanarShadow.hpp"

class Bonus : public Box
{
public:
    CREATE_FUNC(Bonus);
    
    Bonus();
    virtual ~Bonus();
    
    PlanarShadow* getShadow() const {return _shadow;}
protected:
    bool init() override;
    void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4& transform, uint32_t flags) override;
    
    PlanarShadow* _shadow;
};