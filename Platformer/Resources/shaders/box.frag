#ifdef GL_ES
varying mediump vec2 v_texture_coord;
#else
varying vec2 v_texture_coord;
#endif
uniform vec3 u_DirLightSourceColor[1];
uniform vec3 u_DirLightSourceDirection[1];

uniform vec3 u_AmbientLightSourceColor;


varying vec3 v_normal;
varying vec4 v_color;

vec3 computeLighting(vec3 normalVector, vec3 lightDirection, vec3 lightColor, float attenuation)
{
    float diffuse = max(dot(normalVector, lightDirection), 0.0);
    vec3 diffuseColor = lightColor  * diffuse * attenuation;
    
    return diffuseColor;
}

void main(void)
{
    vec3 normal  = normalize(v_normal);
    vec4 combinedColor = vec4(u_AmbientLightSourceColor, 1.0);

    vec3 lightDirection = normalize(u_DirLightSourceDirection[0] * 2.0);
    
    combinedColor.xyz += computeLighting(normal, -lightDirection, u_DirLightSourceColor[0], 1.0);
    
    gl_FragColor = v_color * combinedColor;
//    gl_FragColor = texture2D(CC_Texture0, v_texture_coord) * u_color * combinedColor;
}