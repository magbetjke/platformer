attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec3 a_normal;
varying vec2 v_texture_coord;
varying vec3 v_normal;
varying vec4 v_color;

uniform vec4 u_color;
uniform int u_materialId;

void main(void)
{
    float time = sin(CC_Time.y) * 0.8;
    

    gl_Position = CC_MVPMatrix * a_position;
    gl_PointSize = 10.0;
    
    if (u_materialId == 0)
    {
        float temp = (a_texCoord.x + a_texCoord.y) * 0.25;
    //    float r = clamp(sin(CC_Time.x) + temp, 0.0, 0.3);
    //    float g = clamp(sin(CC_Time.y) + temp, 0.5, 0.8);
    //    float b = clamp(sin(CC_Time.z) + temp, 0.5, 1.0);
        float r = 0.1;
        float g = 0.73 - temp * time;
        float b = 0.89 - temp * time;
//        float g = clamp(sin(CC_Time.y) * 0.2 + 0.5, 0.5, 0.8) +  temp;
//        float b = clamp(sin(CC_Time.z) * 0.5 + 0.5, 0.5, 1.0) + temp;
        v_color = vec4(r, g, b, 1.0);
    }
    else
    {
        v_color = u_color;
    }

    v_color = u_color;
    
    
    v_texture_coord = a_texCoord;
    v_texture_coord.y = (1.0 - v_texture_coord.y);
	v_normal = CC_NormalMatrix *a_normal;
    gl_PointSize = 10.0;
}
