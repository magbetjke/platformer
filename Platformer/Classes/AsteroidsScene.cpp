//
//  AsteroidsScene.cpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#include "cocosGUI.h"

#include "AsteroidsScene.hpp"
#include "Box.hpp"
#include "StartGameCMD.hpp"
#include "Water.hpp"
#include "PlanarShadow.hpp"
#include "Hero.hpp"
#include "ConvexPoly.hpp"
#include <limits>

using namespace cocos2d;
using namespace cocos2d::ui;

Scene* AsteroidsScene::createScene()
{
    auto scene = Scene::create();
    
    auto layer = AsteroidsScene::create();
    scene->addChild(layer);

    return scene;
}

AsteroidsScene::~AsteroidsScene()
{
    CC_SAFE_RELEASE(_light);
    CC_SAFE_RELEASE(_ambient);
}

bool AsteroidsScene::init()
{
    if (Layer::init())
    {
        auto size = Director::getInstance()->getVisibleSize();
    
        _light = DirectionLight::create(Vec3(-0.f, -1.f, -.0f), Color3B::WHITE);
        _light->retain();
        auto angle = Vec3(10.0f, 0.0f, 10.0f);
//        _light->runAction(RepeatForever::create(RotateBy::create(0.5f, angle)));
        
        addChild(_light);
        
        _ambient = AmbientLight::create(Color3B(55, 55, 55));
        _ambient->retain();
        addChild(_ambient);
        
        this->scheduleUpdate();
        

//        poly->runAction(
//                Sequence::createWithTwoActions(
//                        DelayTime::create(1.5f),
//                        CallFunc::create(
//                            [poly]
//                            {
//                                auto angle = Vec3(10.0f, 0.0f, 10.0f);
//                                poly->runAction(RepeatForever::create(RotateBy::create(0.5f, angle)));
//                                }
//                            )
//                    
//                        )
//                );
//        addChild(poly);
        const size_t count = 100;
        Vec2 center {size.width * 0.5f, size.height * 0.5f};
        for (size_t i = 0; i != count; i++)
        {
            auto poly = ConvexPoly::create(10);
//        poly->setScale(20);
            Vec2 pos {size.width * i / count, size.height * (i % 2 ? 0.25f : 0.75f)};
            poly->setPosition3D(Vec3(pos.x, pos.y, 0.0f));
            poly->setColor(Color3B::YELLOW);

            poly->_velocity = (center - pos) * 0.4f;
            _asteroids.push_back(poly);
            addChild(poly);
        }
        
//        auto box = Hero::create();
//        box->setVolume(Vec3(100.0f, 100.0f, 100.0f));
//        box->setColor(Color3B::RED);
//        box->setPosition3D(Vec3(size.width * 0.5f, size.height * 0.5f, 0.0f));
//        box->runAction(
//                Sequence::createWithTwoActions(
//                        DelayTime::create(1.5f),
//                        CallFunc::create(
//                            [box]
//                            {
//                                auto angle = Vec3(10.0f, 0.0f, 10.0f);
//                                box->runAction(RepeatForever::create(RotateBy::create(0.5f, angle)));
//                                }
//                            )
//                    
//                        )
//                );
//        addChild(box);
//        
// 
//        auto button = Button::create();
//        button->setTitleFontSize(70);
//        button->setTitleText("PLAYSPD");
//        button->setOpacity(0);
//        button->setColor(Color3B(50, 220, 200));
//        button->setTitleFontName("fonts/edundot.ttf");
//        button->setPosition(Vec2(size.width * 0.3f, size.height * 0.25f));
//        button->runAction(
//                Sequence::createWithTwoActions(
//                        DelayTime::create(2.5f),
//                        FadeIn::create(1.0f)
//                        )
//                );
//        button->setPressedActionEnabled(true);
//        button->addClickEventListener([](Ref*)
//                {
//                    CommandBus::getInstance()->addCommand(StartGameCMD::create(Mechanic::SPEED_CHANGE));
//                }
//        );
//        addChild(button);
//        
//        auto button2 = Button::create();
//        button2->setTitleFontSize(70);
//        button2->setTitleText("PLAYALT");
//        button2->setOpacity(0);
//        button2->setColor(Color3B(50, 220, 200));
//        button2->setTitleFontName("fonts/edundot.ttf");
//        button2->setPosition(Vec2(size.width * 0.7f, size.height * 0.25f));
//        button2->runAction(
//                Sequence::createWithTwoActions(
//                        DelayTime::create(2.5f),
//                        FadeIn::create(1.0f)
//                        )
//                );
//        button2->setPressedActionEnabled(true);
//        button2->addClickEventListener([](Ref*)
//                {
//                    CommandBus::getInstance()->addCommand(StartGameCMD::create(Mechanic::ALTITUDE_CHANGE));
//                }
//        );
//        addChild(button2);
    
        return true;
    }

    return false;
}

static bool compareAsteroidsPosition(Node* a, Node* b)
{
    return a->getPositionX() < b->getPositionX();
}

void AsteroidsScene::update(float delta)
{
//    std::sort(_asteroids.begin(), _asteroids.end(), compareAsteroidsPosition);
    
    auto size = Director::getInstance()->getVisibleSize();

        for (auto asteroid : _asteroids)
    {
        asteroid->setColor(Color3B::WHITE);
    
        auto new_position = asteroid->getPosition() + asteroid->_velocity * delta;
        if (new_position.x - asteroid->_radius > size.width)
        {
            new_position.x = 0.0f - asteroid->_radius;
        }
        else if (new_position.x + asteroid->_radius < 0.0f)
        {
            new_position.x = size.width + asteroid->_radius;
        }
        
        if (new_position.y - asteroid->_radius > size.height)
        {
            new_position.y = 0.0f - asteroid->_radius;
        }
        else if (new_position.y + asteroid->_radius < 0.0f)
        {
            new_position.y = size.height + asteroid->_radius;
        }
        
        asteroid->setPosition(new_position);
    }

    for (auto asteroid : _asteroids)
    {
        for (auto second : _asteroids)
        {
            if (second != asteroid)
            {
                auto radii_sum = second->_radius + asteroid->_radius;
                if (radii_sum >= (asteroid->getPosition() - second->getPosition()).length())
                {
                    second->setColor(Color3B::GREEN);
                    asteroid->setColor(Color3B::GREEN);
                    checkCollision(asteroid, second);
                }
            }
        }
    }
    

    
}

void AsteroidsScene::checkCollision(ConvexPoly *a, ConvexPoly *b)
{
    auto a_b = b->getPosition() - a->getPosition();
    if (a->_velocity.dot(a_b) > 0.0f)
    {
        Vec2 collideN;
        if (polyIntersects(a, b, collideN))
        {
            a->setColor(Color3B::BLUE);
            b->setColor(Color3B::BLUE);

//            V’ = V – (2 * (V . N)) * N
            collideN.normalize();
            
            CCLOG("normal x = %f y = %f", collideN.x, collideN.y);
            
            auto relative_v = a->_velocity - b->_velocity;
            auto vn = relative_v.dot(collideN) * collideN;
            auto vt = relative_v - vn;
            
            float friction = 0.0f;
            
            if (vt.length() < 0.01f)
                friction = 1.01f;
            
            float elasticity = 0.8f;
            auto new_v = vt * -friction + vn * -(1 + elasticity);
            
//            a->_velocity += new_v * 0.5f;
//            b->_velocity -= new_v * 0.5f;
            
            a->_velocity *= -1;
            b->_velocity *= -1;
//            Vector V = Va – Vb; // relative velocity
//            Vn = (V . N) * N;
//            Vt = V – Vn;
//
//            if (Vt.Length() < 0.01f) friction = 1.01f;
//
//            // response
//            V’ = Vt * -(friction) + Vn  * -(1 + elasticity);
//
//            Va += V’ * 0.5f;
//            Vb -= V’ * 0.5f;
        }
    }
    
}

bool AsteroidsScene::polyIntersects(ConvexPoly* a, ConvexPoly* b, Vec2& collisionNormal)
{
    float overlap = std::numeric_limits<float>::max();
    Vec2 normals[32];
    int index = 0;
    
    for(size_t j = a->_numVerticies - 1, i = 0; i < a->_numVerticies; j = i, i++)
    {
        Vec3 edge = a->getGlobalVertexPos(i) - a->getGlobalVertexPos(j);//A.vertex[I] – A.vertex[J];
        Vec2 normal = Vec2(-edge.y, edge.x);
        normals[index++] = normal;

        if (axisSeparatePolygons(normal, a, b, collisionNormal, overlap))
        {
            return false;
        }
        
    }
    
    collisionNormal = findMTD(normals, index);
    
    for(size_t j = b->_numVerticies - 1, i = 0; i < b->_numVerticies; j = i, i++)
    {
        Vec3 edge = b->getGlobalVertexPos(i) - b->getGlobalVertexPos(j);//A.vertex[I] – A.vertex[J];
        Vec2 normal = Vec2(-edge.y, edge.x);

        if (axisSeparatePolygons(normal, a, b, collisionNormal, overlap))
        {
            return false;
        }
    }
    
//    collisionNormal = normal;
    return true;
}

bool AsteroidsScene::axisSeparatePolygons(cocos2d::Vec2& normal, ConvexPoly* a, ConvexPoly* b, Vec2& collisionNormal, float& overlap)
{
    float mina, maxa;
    float minb, maxb; 

    calculateInterval(normal, a, mina, maxa);
    calculateInterval(normal, b, minb, maxb);

    if (mina > maxb || minb > maxa) 
        return true;

    // find the interval overlap
    float d0 = maxa - minb;
    float d1 = maxb - mina; 
    float depth = (d0 < d1)? d0 : d1;
//
//    // convert the separation axis into a push vector (re-normalise 
//    // the axis and multiply by interval overlap) 
    float axis_length_squared = normal.dot(normal);

    normal *= depth / axis_length_squared;
    return false;
}

Vec2 AsteroidsScene::findMTD(Vec2* push, int num)
{ 
    Vec2 mtd = push[0];
    float mind2 = push[0].dot(push[0]);
    for(int i = 1; i < num; i++)
    { 
        float d2 = push[i].dot(push[i]);
        if (d2 < mind2) 
        { 
            mind2 = d2; 
            mtd = push[i];
        } 
    } 
    return mtd;
}

void AsteroidsScene::calculateInterval(const Vec2& axis, ConvexPoly* poly, float& min, float& max)
{
    Vec3 axis_ {axis.x, axis.y, 0.0f};
    
    float d = axis_.dot(poly->getGlobalVertexPos(0));
    min = max = d; 
    for(size_t i = 0; i < poly->_numVerticies; i++)
    { 
        float d = poly->getGlobalVertexPos(i).dot(axis_);
        if (d < min) 
            min = d; 
        else if(d > max)
            max = d; 
    }
}

