/**
 *
 * Water.cpp
 *
 * @author a.eremin
 *
 */

#include "Water.hpp"

using namespace cocos2d;

const std::string Water::WATER_SHADER = "WaterShader";

float Water::getStep() {return _step;}
void Water::setStep(float value)
{
    if (_step != value)
    {
        _step = value;
        
        _gridIsDirty = true;
    }
}

int Water::getRows() {return _rows;}
void Water::setRows(int value)
{
    if (_rows != value)
    {
        _rows = value;
        
        _gridIsDirty = true;
    }
}

int Water::getCols() {return _cols;}
void Water::setCols(int value)
{
    if (_cols != value)
    {
        _cols = value;
        
        _gridIsDirty = true;
    }
}

const Vec2& Water::getGridSize() const {return _gridSize;}
void Water::setGridSize(const Vec2& value)
{
    if (_gridSize != value)
    {
        _gridSize = value;
        
        _sizeIsDirty = true;
    }
}

GLenum Water::getPrimitiveType() {return _primitiveType;}
void Water::setPrimitiveType(GLenum value)
{
    if (_primitiveType != value)
    {
        _primitiveType = value;
        
        _gridIsDirty = true;
    }
}

float Water::getUVRepeatRatio() {return _uvRepeatRatio;}
void Water::setUVRepeatRatio(float value)
{
    if (_uvRepeatRatio != value)
    {
        _uvRepeatRatio = value;
        
        _gridIsDirty = true;
    }
}

float Water::getWaveLength() {return _length;}
void Water::setWaveLength(float value)
{
    if (_length != value)
    {
        _length = value;
        
        _frequency = 2.0 * M_PI / _length;
        _phase = _speed * _frequency;
    }
}


float Water::getWaveSpeed() {return _speed;}
void Water::setWaveSpeed(float value)
{
    if (_speed != value)
    {
        _speed = value;
        
        
        _phase = _speed * _frequency;
    }
}

void Water::setTexture(const std::string& path)
{
    _texturePath = path;
    
    if (_mesh)
    {
        auto cached = Director::getInstance()->getTextureCache()->addImage(_texturePath);
        cached->setTexParameters({GL_NEAREST, GL_LINEAR, GL_REPEAT, GL_REPEAT});
        _mesh->setTexture(cached);
    }
}

Water* Water::create(const Vec2& value)
{
    Water *pRet = new(std::nothrow) Water();
    if (pRet && pRet->init())
    {
        pRet->autorelease();
        pRet->setGridSize(value);
        return pRet;
    }
    else
    {
        delete pRet;
        pRet = nullptr;
        return nullptr;
    }
}

Water::Water()
    :_mesh(nullptr)
    ,_state(nullptr)
    ,_gridSize(Vec2{10.0f, 10.0f})
    ,_step(20.0f)
    ,_cols(1)
    ,_rows(1)
    ,_primitiveType(GL_TRIANGLES)
    ,_direction(Vec2{1.0f, 0.0f})
    ,_uvRepeatRatio(0.2)
    ,_amplitude(10.0f)
    ,_steepness(0.3f)
    ,_texturePath("textures/water.png")
    ,_gridIsDirty(true)
    ,_sizeIsDirty(true)
    ,_uvOffset(Vec2::ZERO)
    ,_uvSpeed(500.0f)
{
    setWaveSpeed(300.0f);
    setWaveLength(210.0f);
}

Water::~Water()
{
    CC_SAFE_RELEASE(_mesh);
    CC_SAFE_RELEASE(_state);
}

bool Water::init()
{
    if (Sprite3D::init())
    {
        glLineWidth(2);
        
        _shaderUsingLight = true;
    
        GLProgram* shader = GLProgramCache::getInstance()->getGLProgram(WATER_SHADER);
    
        if (!shader)
        {
            shader = GLProgram::createWithFilenames("shaders/water_surface.vert", "shaders/water_surface.frag");
        }
        
        _state = GLProgramState::create(shader);
        _state->retain();
        
        return true;
    }
    
    return false;
}

void Water::visit(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    if (_sizeIsDirty)
    {
        updateVolume();
        
        _sizeIsDirty = false;
    }
    
    if (_gridIsDirty)
    {
        buildMesh();
        
        _gridIsDirty = false;
    }
    
    Sprite3D::visit(renderer, transform, flags);
    
}

void Water::onEnter()
{
    this->scheduleUpdate();
    Sprite3D::onEnter();
}

void Water::onExit()
{
    this->unscheduleUpdate();
    Sprite3D::onExit();
}

void Water::update(float delta)
{
    auto size = _cols * _step;
    Vec2 temp = _direction * delta * _uvSpeed / size;
    
    _uvOffset.x = _uvOffset.x + temp.x;
    _uvOffset.y = _uvOffset.y + temp.y;
    
    if (_uvOffset.x > 1.0f)
        _uvOffset.x -= 1.0f;
    else if (_uvOffset.x < -1.0f)
        _uvOffset.x += 1.0f;
    
    if (_uvOffset.y > 1.0f)
        _uvOffset.y -= 1.0f;
    else if (_uvOffset.x < -1.0f)
        _uvOffset.y += 1.0f;
}

void Water::draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags)
{
    //todo batch
    
    _state->setUniformFloat("u_amplitude", _amplitude);
    _state->setUniformFloat("u_frequency", _frequency);
    _state->setUniformFloat("u_phase", _phase);
    _state->setUniformFloat("u_steepness", _steepness);
    _state->setUniformVec2("u_direction", _direction);
    _state->setUniformVec2("u_uv_offset", _uvOffset);

//    _state->setUniformVec2("u_depth", _direction);

    Sprite3D::draw(renderer, transform, flags);
}

void Water::buildQuad(const cocos2d::Vec2& tl, const float& step, const Vec4& uv, std::vector<float>& positions, std::vector<float>& normals, std::vector<float>& texs, Mesh::IndexArray& indices)
{
    unsigned short start_index = 0;
    
    if (indices.size() > 0)
    {
        start_index = indices[indices.size() - 1] + 1;
    }

    for (int i = 0; i != 2; i++)
    {
        for (int j = 0; j != 2; j++)
        {
                positions.push_back(tl.x + i * step); //x
                positions.push_back(tl.y - j * step); //y
                positions.push_back(0.0f); //z
            
            
                normals.push_back(0.0f); //x
                normals.push_back(0.0f); //y
                normals.push_back(1.0f); //z
        }
    }
    
    texs.push_back(uv.x);texs.push_back(uv.y);
    texs.push_back(uv.x);texs.push_back(uv.w);
    texs.push_back(uv.z);texs.push_back(uv.y);
    texs.push_back(uv.z);texs.push_back(uv.w);
    
    if (_primitiveType == GL_LINES)
    {
        indices.push_back(start_index);
        indices.push_back(start_index + 1);
        
//        indices.push_back(start_index + 1);
//        indices.push_back(start_index + 2);
        
        indices.push_back(start_index + 2);
        indices.push_back(start_index + 3);
        
        indices.push_back(start_index);
        indices.push_back(start_index + 2);
        
//        indices.push_back(start_index);
//        indices.push_back(start_index + 3);
        
        indices.push_back(start_index + 1);
        indices.push_back(start_index + 3);
    }
    else
    {
        indices.push_back(start_index);
        indices.push_back(start_index + 1);
        indices.push_back(start_index + 2);
        
        indices.push_back(start_index + 2);
        indices.push_back(start_index + 1);
        indices.push_back(start_index + 3);
    }
}


void Water::buildMesh()
{

    if (_mesh)
    {
        //remove old
        auto vertex_data = _mesh->getMeshIndexData()->getMeshVertexData();
        auto vertex_data_index = std::find(_meshVertexDatas.begin(), _meshVertexDatas.end(), vertex_data);
        if (vertex_data_index != _meshVertexDatas.end())
        {
            _meshVertexDatas.erase(vertex_data_index);
            
        }
    
        auto mesh_index = std::find(_meshes.begin(), _meshes.end(), _mesh);
        
        if (mesh_index != _meshes.end())
        {
            _meshes.erase(mesh_index);
            
        }
        
        CC_SAFE_RELEASE(_mesh);
    }
    
    std::vector<float> positions;
    std::vector<float> normals;
    std::vector<float> texs;
    Mesh::IndexArray indices;
    
   
    auto start_x = - _cols * _step * 0.5f;
    auto start_y = _rows * _step * 0.5f;
    
    float uv_size = _cols * _step * _uvRepeatRatio;
    
    Vec4 uv;
    float uv_step = _step / uv_size;
    for (int i = 0; i != _cols; i++)
    {
        for (int j = 0; j != _rows; j++)
        {
            uv.x = uv_step * i;
            uv.y = uv_step * j;
            uv.z = uv_step * (i + 1);
            uv.w = uv_step * (j + 1);
            
            buildQuad(Vec2(start_x + i * _step, start_y - j * _step), _step, uv, positions, normals, texs, indices);
        }
    }
    
    _mesh = Mesh::create(positions, normals, texs, indices);
    
    _mesh->getMeshIndexData()->setPrimitiveType(_primitiveType);
    _mesh->retain();
    
    setTexture(_texturePath);
    this->addMesh(_mesh);
        
    applyVertexAttribs();
}

void Water::applyVertexAttribs()
{

    setGLProgramState(_state);
        
    auto attributeCount = _mesh->getMeshVertexAttribCount();
    
    long offset = 0;
    for (auto i = 0; i < attributeCount; i++) {
        auto meshattribute = _mesh->getMeshVertexAttribute(i);
        _state->setVertexAttribPointer(s_attributeNames[meshattribute.vertexAttrib],
            meshattribute.size,
            meshattribute.type,
            GL_FALSE,
            _mesh->getVertexSizeInBytes(),
            (GLvoid*)offset);
        offset += meshattribute.attribSizeBytes;
    }
}

void Water::updateVolume()
{
    auto width = _cols * _step;
    auto height = _rows * _step;
    
    float scale_x = _gridSize.x / width;
    float scale_y = _gridSize.y / height;
    
    setScaleX(getScaleX() * scale_x);
    setScaleY(getScaleY() * scale_y);
    setScaleZ(getScaleZ() * std::max(scale_x, scale_y));
}