//
//  StartGameCMD.cpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#include "StartGameCMD.hpp"


void StartGameCMD::execute()
{
    auto director = cocos2d::Director::getInstance();
    
    director->replaceScene(GameScene::createScene(_mechanic));
    Command::execute();
}