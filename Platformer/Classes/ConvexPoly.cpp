/**
 *
 * ConvexPoly.cpp
 *
 * @author a.eremin
 *
 */

#include "ConvexPoly.hpp"
#include "Box.hpp"

using namespace cocos2d;

ConvexPoly* ConvexPoly::create(int num)
{
    ConvexPoly* ptr = new (std::nothrow) ConvexPoly(num);
    if (ptr)
    {
        ptr->autorelease();
        ptr->init();
    }
    
    return ptr;
}

Vec3 ConvexPoly::getGlobalVertexPos(size_t i)
{
    CC_ASSERT(i < _numVerticies);
    
    return getNodeToWorldTransform() * _verticies[i];
}

ConvexPoly::ConvexPoly(int num)
    :_numVerticies(num)
    ,_primitiveType(GL_LINE_LOOP)
    ,_radius(10.0f)
{
}

ConvexPoly::~ConvexPoly()
{
}

bool ConvexPoly::init()
{
    if (Sprite3D::init())
    {
        glLineWidth(5);
    
        buildMesh();
        this->addMesh(_mesh);
        
        applyShader();
        
        return true;
    }
    
    return false;
}

void ConvexPoly::buildMesh()
{
    std::vector<float> positions;
    std::vector<float> normals;
    std::vector<float> texs;
    Mesh::IndexArray indices;
    
    const float half_width = _radius;
    float width  = half_width * 2;
    float step = width * 2 / _numVerticies;
    
    std::random_device rd;
    
    std::default_random_engine engine(rd());
    std::uniform_real_distribution<float> distr(-0.2f, 0.2f);
    
    float prev_x = 0.0f;
    float prev_y = 0.0f;
    
    float startAngle = 0.0f;
    float const two_pi = 6.28318530718f;
    float wiggle_range = 10.0f;
    
    for (int i = 0; i != _numVerticies; i++)
    {
        CCLOG("prev_x = %f", prev_x);
        CCLOG("prev_y = %f", prev_y);
        
        float angle = startAngle + (two_pi / _numVerticies) * i + 0.2f/*distr(engine)*/;
        float wiggle = 0.0f;//(distr(engine) * wiggle_range) - (wiggle_range / 2);
        
        auto x = (half_width + wiggle) * std::cos(angle);
        auto y = -(half_width + wiggle) * std::sin(angle);
        _verticies.push_back(Vec3(x, y, 0.0f));
        
        positions.push_back(x); //x
        positions.push_back(y); //y
//        if (i <= 5)
//        {
//            prev_x += (width - prev_x) * distr(engine);
//            prev_y += (width - prev_y) * distr(engine);
//            positions.push_back(0.0f + prev_x); //x
//            positions.push_back(0.0f + prev_y); //y
//        }
//        else
//        {
//            prev_x = prev_x * distr(engine);
//            prev_y = prev_y * distr(engine);
//            positions.push_back(prev_x); //x
//            positions.push_back(prev_y); //y
//        }
        
//        var ang = startAng + (twoPi / n) * i;
//         var wiggle = (regMode.selected) ? 0 : (random.get() * wiggleRange) - (wiggleRange / 2);
//         vertex[i].x = origin.x + (radius + wiggle) * Math.cos(ang);
//         vertex[i].y = origin.y - (radius + wiggle) * Math.sin(ang);
        
        positions.push_back(0.0f); //z
        
        normals.push_back(0.0f); //x
        normals.push_back(0.0f); //y
        normals.push_back(1.0f);
        
        texs.push_back(0.0);texs.push_back(0.0);
        
        indices.push_back(i);
    }
    
//    indices.push_back(0);
    
//    if (_primitiveType == GL_LINES)
//    {
//        indices.push_back(0);
//        indices.push_back(1);
//        
////        indices.push_back(start_index + 1);
////        indices.push_back(start_index + 2);
//        
//        indices.push_back(2);
//        indices.push_back(3);
//        
//        indices.push_back(0);
//        indices.push_back(2);
//        
////        indices.push_back(start_index);
////        indices.push_back(start_index + 3);
//        
//        indices.push_back(1);
//        indices.push_back(3);
//    }
//    else
//    {
//        indices.push_back(0);
//        indices.push_back(1);
//        indices.push_back(2);
//        
//        indices.push_back(2);
//        indices.push_back(1);
//        indices.push_back(3);
//    }

    _mesh = Mesh::create(positions, normals, texs, indices);
    _mesh->getMeshIndexData()->setPrimitiveType(_primitiveType);
}

void ConvexPoly::applyShader()
{
    _shaderUsingLight = true;

    setGLProgramState(GLProgramState::create(getOrCreateShader()));

//    _glProgramState->getUniformCount();
    
//    _glProgramState->updateUniformsAndAttributes();

//    _meshCommand.genMaterialID(0, _glProgramState, _mesh->getMeshIndexData()->getVertexBuffer()->getVBO(), _mesh->getMeshIndexData()->getIndexBuffer()->getVBO(),  BlendFunc::ALPHA_NON_PREMULTIPLIED);
//    
//    _meshCommand.setCullFaceEnabled(true);
//    _meshCommand.setDepthTestEnabled(true);
//    _meshCommand.setSkipBatching(true);
    
//    _mesh->setGLProgramState(_glProgramState);
    
    auto attributeCount = _mesh->getMeshVertexAttribCount();
    
    long offset = 0;
    for (auto i = 0; i < attributeCount; i++) {
        auto meshattribute = _mesh->getMeshVertexAttribute(i);
        _glProgramState->setVertexAttribPointer(s_attributeNames[meshattribute.vertexAttrib],
            meshattribute.size,
            meshattribute.type,
            GL_FALSE,
            _mesh->getVertexSizeInBytes(),
            (GLvoid*)offset);
        offset += meshattribute.attribSizeBytes;
    }
}

GLProgram* ConvexPoly::getOrCreateShader()
{
    GLProgram* shader = GLProgramCache::getInstance()->getGLProgram(Box::BOX_SHADER);
    
    if (!shader)
    {
        shader = GLProgram::createWithFilenames("shaders/box.vert", "shaders/box.frag");
    }
    
    return shader;
}

void ConvexPoly::draw(Renderer *renderer, const Mat4 &transform, uint32_t flags)
{
    Sprite3D::draw(renderer, transform, flags);
}