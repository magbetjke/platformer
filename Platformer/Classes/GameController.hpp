//
//  GameController.hpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#pragma once

#include "cocos2d.h"

#include "BlockGenerator.hpp"
#include "GameObjects.hpp"

#define PLATFORM_HEIGHT 200
#define PLATFORM_DEPTH 100
//#define ALTITUDE_ASCENDANCE_SPEED 10.0f
#define ALTITUDE_BONUS 30.0f
#define SPEED_BONUS 30.0f
#define HERO_DEATH_THRESHOLD -30.0f

class Box;
class Bonus;
class GameScene;
class Hero;
class Water;

struct GameConfig
{
    float speed = 600.0f;
    float minSpeed = 600.0f;
    float speedAcceleration = 10.0f;
    float speedBonus = SPEED_BONUS;
    float spawnDistance = 800.0f;
    float gainScoreSpeed = 10.0f;
    float maxAltitude = PLATFORM_HEIGHT * 0.3f;
    float minAltitude = -PLATFORM_HEIGHT;
    float altitudeAscendanceSpeed = 0.0f;//10
    float altitudeBonus = ALTITUDE_BONUS;

};

struct GameModel
{
    float score;
    int coins;
    
    void reset()
    {
        score = 0.0f;
        coins = 0;
    }
};

class GameController final {
public:
    friend class cocos2d::Scheduler;
    
    static const size_t BOXES_LIMIT = 10;
    
    void setScene(GameScene* value);
    
    GameController();
    ~GameController();
    
    void init(GameConfig config = GameConfig());
    void initForTest();
    
private:
    void update(float delta);
    
    void reset();

    void start();
    void stop();
    
    void showStats();

    bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);
    void onTouchEnded(cocos2d::Touch*, cocos2d::Event*);
    
    Box* addBlock(const BlockDescriptor& descr, bool animated = true);

    void updateHero(float delta);
    void updateWorld(float delta);
    void updateScore(float delta);
    void checkHeroJump();
    void checkCollisionAgainstSurface();
    bool checkCollisionAgainstWalls();
    void checkCollisionAgainstPowerups();
    void collectPowerUp(Box* powerup);

    cocos2d::EventListenerTouchOneByOne* _touchListener;

    GameScene* _scene;
    Hero* _hero;
    cocos2d::Node* _world;
    Water* _water;
    Water* _sky;
    
    
    GameObjects<Box, 10> _groundObjs;
    GameObjects<Bonus, 10> _powerupObjs;
    
    BlockGenerator _generator;

    GameModel _model;
    GameConfig _gameConfig;
   
    bool _firstLaunch;
    bool _pendingToStart;
    bool _running;
    
    float _spawnDistance;
    float _speed; //x per second
    float _currentSpeed;
    float _gainScoreSpeed; //per second
    
    float _heroVerticalVelocity;
    float _heroLandedY;
    
    float _currentAltitude;
    float _altitudeAscendanceSpeed;
    bool _heroDrawn;
    
    BlockDescriptor _currentBlock;
    float _currenSpawnPoint;
    
    bool _touched;

};
