//
//  GameScene.hpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#pragma once
#include "cocos2d.h"

#include "GameController.hpp"
#include "Hero.hpp"
#include "GameOverView.hpp"
#include "Water.hpp"

class Box;

//test
enum class Mechanic
{
    ALTITUDE_CHANGE,
    SPEED_CHANGE
};

class GameScene final : public cocos2d::Layer
{
public:
    CREATE_FUNC(GameScene)
    static cocos2d::Scene* createScene(Mechanic value);
    
    void setMechanic(Mechanic value);
    
    cocos2d::Node* getWorld() const {return _gameObjsContainer;}
    cocos2d::Camera* getWorldCamera() const {return _worldCamera;}
    Hero* getHero() const {return _hero;}
    Water* getWater() const {return _water;}
    Water* getSky() const {return _sky;}
    void setUIAltitude(int value);
    void setUISpeed(int value);
    
    void showHint();
    void hideHint();
    
    GameOverView* getGameOverView() const {return _gameOver;}
    
    void addObjectToWorld(cocos2d::Node* value);
    
    GameScene();
    ~GameScene();
protected:
    bool init() override;
    
private:
    GameController _controller;
    
    cocos2d::Node* _gameObjsContainer;
    Water* _water;
    Water* _sky;
    
    cocos2d::Node* _sceneContainer;
    cocos2d::Node* _guiContainer;
    GameOverView* _gameOver;
    cocos2d::ui::Text* _hint;
    cocos2d::ui::Text* _altitude;
    cocos2d::ui::Text* _speed;
    
    cocos2d::DirectionLight* _light;
    cocos2d::AmbientLight* _ambient;
    
    cocos2d::Camera* _worldCamera;
    
    Hero* _hero;
};
