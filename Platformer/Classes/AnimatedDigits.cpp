//
//  AnimatedDigits.cpp
//  Platformer
//
//  Created by Алексей Еремин on 04/05/15.
//
//

#include "AnimatedDigits.hpp"

using namespace cocos2d;

AnimatedDigits* AnimatedDigits::create(const std::string &fontName, int fontSize)
{
    AnimatedDigits *text = new (std::nothrow) AnimatedDigits;
    if (text && text->init("", fontName, fontSize))
    {
        text->autorelease();
        return text;
    }
    CC_SAFE_DELETE(text);
    return nullptr;
}

AnimatedDigits::AnimatedDigits()
    :_startNumber(0)
    ,_finishNumber(0)
    ,_running(false)
    ,_duration(0.0f)
    ,_timePassed(0.0f)
{
    
}

AnimatedDigits::~AnimatedDigits()
{
    stop();
}


void AnimatedDigits::setStartNumber(int value)
{
    _startNumber = value;
    
    setString(std::to_string(value));
}

void AnimatedDigits::setFinishNumber(int value)
{
    _finishNumber = value;
}

void AnimatedDigits::run(float duration)
{
    if (_running) return;
    
    CCASSERT(_startNumber <= _finishNumber, "_startNumber must be less than _finishNumber");
    
    _running = true;
    
    _duration = duration;
    
    _timePassed = 0.0f;
    
    Director::getInstance()->getScheduler()->scheduleUpdate(this, 1, false);
}

void AnimatedDigits::update(float delta)
{
    _timePassed += delta;
    
    if (_timePassed >= _duration)
    {
        setString(std::to_string(_finishNumber));
        stop();
    }
    else
    {
        setString(std::to_string(static_cast<int>(MathUtil::lerp(_startNumber, _finishNumber, _timePassed / _duration))));
    }
}

void AnimatedDigits::stop()
{
    _running = false;
    
    Director::getInstance()->getScheduler()->unscheduleUpdate(this);
}