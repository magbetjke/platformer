//
//  Hero.hpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#pragma once

#include "cocos2d.h"
#include "Box.hpp"
#include "ShadowVolume.hpp"

class Hero : public Box
{
public:
    CREATE_FUNC(Hero)
    
    Hero();
    virtual ~Hero();
    
    enum class State
    {
        FALL,
        ON_SURFACE,
        JUMP_UP,
        JUMP_DOWN
    };
    
    State getState() const {return _state;}
    void setState(State value);
    
    void setVolume(const cocos2d::Vec3& value) override;
    
    bool isOnSurface() const {return _state == State::ON_SURFACE;}
    bool isFalling() const {return _state == State::FALL;}
    bool isJumpingUP() const {return _state == State::JUMP_UP;}
    bool isJumpingDown() const {return _state == State::JUMP_DOWN;}
    
    float getAscensionInitialSpeed() const {return _ascensionInitialSpeed;}
    float getAscensionAcceleration() const {return _ascensionAcceleration;}
    float getFallingAcceleration() const {return _fallingAcceleration;}
    
    float getMinJumpHeight() const {return _minJumpHeight;}
    float getMaxJumpHeight() const {return _maxJumpHeight;}
protected:
    bool init() override;
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;

private:
//    cocos2d::Quaternion rotationBetweenVectors(cocos2d::Vec3 start, cocos2d::Vec3 dest);

    State _state;
//    Box* _shadowVolume;
    Box* _shadowPenumbra;
    
    float _ascensionInitialSpeed;
    float _ascensionAcceleration;
    float _fallingAcceleration;

    float _minJumpHeightInUnits;
    float _maxJumpHeightInUnits;
    float _minJumpHeight;
    float _maxJumpHeight;
};

