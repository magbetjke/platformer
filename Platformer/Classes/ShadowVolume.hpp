/**
 *
 * ShadowVolume.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include "cocos2d.h"
#include "Box.hpp"

//todo make node
class ShadowVolume : public Box
{
public:
    CREATE_FUNC(ShadowVolume);
    ShadowVolume();
    
protected:
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    void buildMesh() override;
    cocos2d::GLProgram* getOrCreateShader() override;
    
private:
    static const std::string SHADOW_SHADER;
    void changeRenderState();
    void restoreRenderState();
    void onDraw();
    void drawMesh();
    cocos2d::Quaternion rotationBetweenVectors(cocos2d::Vec3 start, cocos2d::Vec3 dest);
    
    GLboolean _currentStencilEnabled;
    GLuint _currentStencilWriteMask;
    GLenum _currentStencilFunc;
    GLint _currentStencilRef;
    GLuint _currentStencilValueMask;
    GLenum _currentStencilFail;
    GLenum _currentStencilPassDepthFail;
    GLenum _currentStencilPassDepthPass;
    GLboolean _currentDepthWriteMask;
    
    GLboolean _cullFaceEnabled;
    GLboolean _depthTestEnabled;
    GLint _cullFaceMode;
    GLboolean _depthMask;

    cocos2d::CustomCommand _renderCommand;
    cocos2d::GroupCommand _groupCommand;
};