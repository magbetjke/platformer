/**
 *
 * CommandBus.cpp
 *
 * @author Alexey Eremin
 *
 */

#include "CommandBus.hpp"
#include "cocos2d.h"


CommandBus* CommandBus::_instance = nullptr;

CommandBus* CommandBus::getInstance()
{
    if (!_instance)
    {
        _instance = new CommandBus();
    }
    
    return _instance;
}

CommandBus::CommandBus()
:_busy(false)
{
    
}

void CommandBus::runNextCommand()
{
    if (!_busy && !_commands.empty())
    {
        _busy = true;
        
        auto cmd = _commands.back();
        cmd->execute();
    }
}

void CommandBus::addCommand(std::shared_ptr<Command> cmd)
{
    CCLOG("add command %s", cmd->getId());

    cmd->setCommandBus(this);
    cmd->setCompleteHandler(CC_CALLBACK_1(CommandBus::onCompleteCommand, this));
    _commands.push(cmd);
    
    this->runNextCommand();
}

void CommandBus::onCompleteCommand(std::shared_ptr<Command> cmd)
{
    _busy = false;
    
    _commands.pop();
    this->runNextCommand();
}