//
//  AnimatedDigits.hpp
//  Platformer
//
//  Created by Алексей Еремин on 04/05/15.
//
//

#pragma once

#include "cocos2d.h"
#include "cocosGUI.h"

class AnimatedDigits: public cocos2d::ui::Text
{
public:
    friend class cocos2d::Scheduler;

    static AnimatedDigits* create(const std::string &fontName, int fontSize);
    
    AnimatedDigits();
    virtual ~AnimatedDigits();
    
    void setStartNumber(int value);
    void setFinishNumber(int value);
    
    void run(float duration);
protected:
    using cocos2d::ui::Text::setString;
    
private:
    void update(float delta);
    void stop();
    
    int _startNumber;
    int _finishNumber;
    
    bool _running;
    float _duration;
    float _timePassed;
};