//
//  BlockGenerator.hpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#pragma once

#include "cocos2d.h"

struct BlockDescriptor
{
    enum class Type {
        EMPTY,
        SOLID
    } type;
    
    cocos2d::Vec3 size;
    bool containsPowerUp;
};

class BlockGenerator final {
public:
    BlockGenerator();
    ~BlockGenerator();
    
    void setStep(float value) {_step = value;}
    
    void setMaxEmptyLength(int value) {_maxEmptyLength = value;}
    void setMinEmptyLength(int value) {_minEmptyLength = value;}
    void setMaxSolidLength(int value) {_maxSolidLength = value;}
    void setMinSolidLength(int value) {_minSolidLength = value;}
    
    void setBlockHeight(float value) {_blockHeight = value;}
    void setBlockDepth(float value) {_blockDepth = value;}
    
    BlockDescriptor getNextBlock(const BlockDescriptor& current);
    
private:
    cocos2d::Vec3 getVolume(float depth, float height, float step, int minLength, int maxLength);

    int _maxEmptyLength;
    int _minEmptyLength;
    int _maxSolidLength;
    int _minSolidLength;

    float _step;
    float _blockHeight;
    float _blockDepth;
    
};