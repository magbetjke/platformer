//
//  StartScreen.cpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#include "cocosGUI.h"

#include "StartScene.hpp"
#include "Box.hpp"
#include "StartGameCMD.hpp"
#include "Water.hpp"
#include "PlanarShadow.hpp"
#include "Hero.hpp"
#include "ConvexPoly.hpp"

using namespace cocos2d;
using namespace cocos2d::ui;

Scene* StartScene::createScene()
{
    auto scene = Scene::create();
    
    auto layer = StartScene::create();
    scene->addChild(layer);

    return scene;
}

StartScene::~StartScene()
{
    CC_SAFE_RELEASE(_light);
    CC_SAFE_RELEASE(_ambient);
}

bool StartScene::init()
{
    if (Layer::init())
    {
        auto size = Director::getInstance()->getVisibleSize();
    
        _light = DirectionLight::create(Vec3(-0.f, -1.f, -.0f), Color3B::WHITE);
        _light->retain();
        auto angle = Vec3(10.0f, 0.0f, 10.0f);
//        _light->runAction(RepeatForever::create(RotateBy::create(0.5f, angle)));
        
        addChild(_light);
        
        _ambient = AmbientLight::create(Color3B(55, 55, 55));
        _ambient->retain();
        addChild(_ambient);
        
        
        auto poly = ConvexPoly::create(6);
        poly->setScale(20);
        poly->setPosition3D(Vec3(size.width * 0.5f, size.height * 0.5f, 0.0f));
        poly->setColor(Color3B::RED);
        poly->runAction(
                Sequence::createWithTwoActions(
                        DelayTime::create(1.5f),
                        CallFunc::create(
                            [poly]
                            {
                                auto angle = Vec3(10.0f, 0.0f, 10.0f);
                                poly->runAction(RepeatForever::create(RotateBy::create(0.5f, angle)));
                                }
                            )
                    
                        )
                );
        addChild(poly);
        
        return true;
        
        
        auto box = Hero::create();
        box->setVolume(Vec3(100.0f, 100.0f, 100.0f));
        box->setColor(Color3B::RED);
        box->setPosition3D(Vec3(size.width * 0.5f, size.height * 0.5f, 0.0f));
        box->runAction(
                Sequence::createWithTwoActions(
                        DelayTime::create(1.5f),
                        CallFunc::create(
                            [box]
                            {
                                auto angle = Vec3(10.0f, 0.0f, 10.0f);
                                box->runAction(RepeatForever::create(RotateBy::create(0.5f, angle)));
                                }
                            )
                    
                        )
                );
        addChild(box);
        
 
        auto button = Button::create();
        button->setTitleFontSize(70);
        button->setTitleText("PLAYSPD");
        button->setOpacity(0);
        button->setColor(Color3B(50, 220, 200));
        button->setTitleFontName("fonts/edundot.ttf");
        button->setPosition(Vec2(size.width * 0.3f, size.height * 0.25f));
        button->runAction(
                Sequence::createWithTwoActions(
                        DelayTime::create(2.5f),
                        FadeIn::create(1.0f)
                        )
                );
        button->setPressedActionEnabled(true);
        button->addClickEventListener([](Ref*)
                {
                    CommandBus::getInstance()->addCommand(StartGameCMD::create(Mechanic::SPEED_CHANGE));
                }
        );
        addChild(button);
        
        auto button2 = Button::create();
        button2->setTitleFontSize(70);
        button2->setTitleText("PLAYALT");
        button2->setOpacity(0);
        button2->setColor(Color3B(50, 220, 200));
        button2->setTitleFontName("fonts/edundot.ttf");
        button2->setPosition(Vec2(size.width * 0.7f, size.height * 0.25f));
        button2->runAction(
                Sequence::createWithTwoActions(
                        DelayTime::create(2.5f),
                        FadeIn::create(1.0f)
                        )
                );
        button2->setPressedActionEnabled(true);
        button2->addClickEventListener([](Ref*)
                {
                    CommandBus::getInstance()->addCommand(StartGameCMD::create(Mechanic::ALTITUDE_CHANGE));
                }
        );
        addChild(button2);
    
        return true;
    }

    return false;
}