//
//  GoToStartScreenCMD.cpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#include "GoToStartScreenCMD.hpp"
#include "StartScene.hpp"

void GoToStartScreenCMD::execute()
{
    auto director = cocos2d::Director::getInstance();
    director->replaceScene(StartScene::createScene());
    
    Command::execute();
}