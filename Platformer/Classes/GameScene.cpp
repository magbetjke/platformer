//
//  GameScene.cpp
//  Platformer
//
//  Created by Алексей Еремин on 03/05/15.
//
//

#include "GameScene.hpp"

#include "Box.hpp"

using namespace cocos2d;

GameScene::GameScene()
    :_gameObjsContainer(nullptr)
    ,_guiContainer(nullptr)
    ,_light(nullptr)
    ,_ambient(nullptr)
    ,_hero(nullptr)
{
}

GameScene::~GameScene()
{
    CC_SAFE_RELEASE(_light);
    CC_SAFE_RELEASE(_ambient);
}

void GameScene::showHint()
{
    _hint->setOpacity(0);
    _hint->setVisible(true);
    
    _hint->stopAllActions();
    _hint->runAction(FadeIn::create(1.0f));
}

void GameScene::hideHint()
{
    _hint->setVisible(false);
}

void GameScene::addObjectToWorld(cocos2d::Node* value)
{
    value->setCameraMask(_gameObjsContainer->getCameraMask());
    _gameObjsContainer->addChild(value);
}

Scene* GameScene::createScene(Mechanic value)
{
    auto scene = Scene::create();
    
    auto layer = GameScene::create();
    layer->setMechanic(value);
    scene->addChild(layer);

    return scene;
}

void GameScene::setMechanic(Mechanic value)
{
    GameConfig conf;
    if (value == Mechanic::ALTITUDE_CHANGE)
    {
        conf.speed = 600.0f;
        conf.minSpeed = 600.0f;
        conf.speedAcceleration = 0.0f;
        conf.speedBonus = 0.0f;
        conf.spawnDistance = 800.0f;
        conf.gainScoreSpeed = 10.0f;
        conf.maxAltitude = PLATFORM_HEIGHT * 0.3f;
        conf.minAltitude = -PLATFORM_HEIGHT;
        conf.altitudeAscendanceSpeed = 10.0f;
        conf.altitudeBonus = ALTITUDE_BONUS;
    }
    else if (value == Mechanic::SPEED_CHANGE)
    {
        conf.speed = 600.0f;
        conf.minSpeed = 600.0f;
        conf.speedAcceleration = 20.0f;
        conf.speedBonus = SPEED_BONUS;
        conf.spawnDistance = 800.0f;
        conf.gainScoreSpeed = 10.0f;
        conf.maxAltitude = PLATFORM_HEIGHT * 0.3f;
        conf.minAltitude = -PLATFORM_HEIGHT;
        conf.altitudeAscendanceSpeed = 0.0f;
        conf.altitudeBonus = 0.0f;
    }
    
    _controller.init(conf);
}

bool GameScene::init()
{
    if (Layer::init())
    {
        Size visibleSize = Director::getInstance()->getVisibleSize();

                
        float scene_depth = 4000;
        _worldCamera = Camera::createPerspective(70, (GLfloat)visibleSize.width / visibleSize.height, 10, scene_depth);
        Vec3 eye(visibleSize.width * 0.45, visibleSize.height * 1.0, visibleSize.width * 0.5), center(visibleSize.width * 0.45f, visibleSize.height * 0.3f, visibleSize.width * -0.35);
        
        Vec3 up = eye - center;
        up.cross(Vec3(1.0f, 0.0f, 0.0f));
        
        _worldCamera->setPosition3D(eye);
        _worldCamera->lookAt(center, up);
        _worldCamera->setCameraFlag(CameraFlag::USER1);
    
        _ambient = AmbientLight::create(Color3B(155, 155, 155));
        _ambient->retain();
        addChild(_ambient);
    
        _light = DirectionLight::create(Vec3(-0.5f, -1.0f, -0.0f), Color3B(255, 255, 255));
//        _light = DirectionLight::create(Vec3(0.0f, -1.0f, 0.0f), Color3B(255, 255, 255));
        _light->retain();
        this->addChild(_light);

        _sceneContainer = Node::create();
        addChild(_sceneContainer);
        
        auto billboard = BillBoard::create(BillBoard::Mode::VIEW_PLANE_ORIENTED);
        billboard->setPosition3D(Vec3(visibleSize.width * 0.5f, 0.0f, -scene_depth * 0.5f));
        billboard->setAnchorPoint(Vec2::ANCHOR_MIDDLE_BOTTOM);
        
        _sky = Water::create(Vec2(visibleSize.width * 6.0f, visibleSize.height * 2.0f));
        _sky->setPosition3D(Vec3(0.0f, visibleSize.height * 1.1f, 0.0f));
        _sky->setRows(1);
        _sky->setCols(1);
        _sky->setWaveAmplitude(0.0f);
        _sky->setUVRepeatRatio(0.8f);
        _sky->setUVSpeed(0.0f);
        _sky->setTexture("textures/sky_tex_2.jpg");
        
        billboard->addChild(_sky);
        
        _water = Water::create(Vec2(visibleSize.width * 5.0, scene_depth));
        _water->setPosition3D(Vec3(visibleSize.width * 0.5f, visibleSize.height * 0.5f - 50, 0.0f));
        _water->setRotation3D(Vec3(-90.0f, 0.0f, 0.0f));
        _water->setRows(100);
        _water->setCols(110);
        _water->setWaveDirection(Vec2(1.0f, 0.25f));
//        _water->setColor(Color3B(10, 177, 232));
        _water->setColor(Color3B(115, 225, 249));
        _water->setTexture("textures/water_tex_5.jpg");
        _water->setUVRepeatRatio(0.4);
        _water->setWaveSpeed(150.0f);
        _water->setBlendFunc({GL_ONE, GL_DST_COLOR});
//        _water->setColor(Color3B::YELLOW);
       
    
        _gameObjsContainer = Node::create();
        _gameObjsContainer->setPosition3D(Vec3(visibleSize.width * 0.5f, visibleSize.height * 0.5f, 0.0f));
        _sceneContainer->addChild(_gameObjsContainer);
        
        _sceneContainer->addChild(_water);
        _sceneContainer->addChild(billboard);
        
        _hero = Hero::create();
        _hero->setColor(Color3B::RED);
        _hero->setVolume(Vec3(50, 50, 50));
        _gameObjsContainer->addChild(_hero, 1000);
        
        _guiContainer = Node::create();
        addChild(_guiContainer);
        
        _guiContainer->setPosition(visibleSize.width * 0.5f, visibleSize.height * 0.5f);
        
        _hint = ui::Text::create("TAP TO START", "fonts/arial.ttf", 25);
        _hint->setPosition(Vec2(0.0f, -visibleSize.height * 0.4));
        _hint->setVisible(false);
        _guiContainer->addChild(_hint);
        
        _altitude = ui::Text::create("ALT: ", "fonts/editundo.ttf", 60);
        _altitude->setPosition(Vec2(0.0f, visibleSize.height * 0.3));
        _altitude->setColor(Color3B::GREEN);
        _altitude->setVisible(true);
        _guiContainer->addChild(_altitude);
        
        _speed = ui::Text::create("SPD: ", "fonts/editundo.ttf", 60);
        _speed->setPosition(Vec2(0.0f, visibleSize.height * 0.4));
        _speed->setColor(Color3B::GREEN);
        _speed->setVisible(true);
        _guiContainer->addChild(_speed);
        
        _gameOver = GameOverView::create();
        _guiContainer->addChild(_gameOver);
        
        addChild(_worldCamera);
//        _worldCamera->setRotation3D(Vec3(1.0f, 0.0f, 0.0f));
//        _sceneContainer->setRotation3D(Vec3(-20.0f, 0.0f, 0.0f));
        _sceneContainer->setCameraMask(static_cast<int>(CameraFlag::USER1));
        _guiContainer->setCameraMask(static_cast<int>(CameraFlag::DEFAULT));
        
        _controller.setScene(this);
        _controller.init();
        
        return true;
    }
    
    return false;
}

void GameScene::setUIAltitude(int value)
{
    _altitude->setString("ALT: " + std::to_string(value));
}

void GameScene::setUISpeed(int value)
{
    _speed->setString("SPD: " + std::to_string(value));
}

