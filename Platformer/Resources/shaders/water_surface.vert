uniform float u_amplitude;
uniform float u_frequency;
uniform float u_phase;
uniform float u_steepness;
uniform vec2 u_direction;
uniform float u_depth;
#ifdef GL_ES
uniform mediump vec2 u_uv_offset;
varying mediump vec2 v_texture_coord;
#else
uniform vec2 u_uv_offset;
varying vec2 v_texture_coord;
#endif
attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec3 a_normal;


varying vec3 v_normal;

void main(void)
{
    //gerstner
    float time = CC_Time.y;
    float temp = u_steepness * u_amplitude * cos(u_frequency * dot(u_direction, a_position.xy) + u_phase * time);
    vec4 modified_position = a_position;
    modified_position.x += u_direction.x * temp;
    modified_position.y += u_direction.y * temp;
    modified_position.z = u_amplitude * sin(dot(u_direction, a_position.xy) * u_frequency + time * u_phase);

//    vec2 offset = u_direction * time * u_phase / u_frequency * 0.5;
    v_texture_coord = a_texCoord + u_uv_offset;
//    v_texture_coord.y = (1.0 - v_texture_coord.y);
    
    float S = sin(u_frequency * dot(u_direction, modified_position.xy) + u_phase * time);
    float C = cos(u_frequency * dot(u_direction, modified_position.xy) + u_phase * time);
    float WA = u_frequency * u_amplitude;
    
    vec3 normal = vec3(
        -u_direction.x * WA * C,
        -u_direction.y * WA * C,
        1.0 - u_steepness * WA * S
        );
    
	v_normal = (normal - a_normal) * CC_NormalMatrix;
    
    gl_Position = CC_MVPMatrix * modified_position;
    gl_PointSize = 10.0;
//    v_alpha = gl_Position.w;
}
