//
//  Boids.cpp
//  Platformer
//
//  Created by Алексей Еремин on 19/01/16.
//
//

#include "Boids.hpp"

void Boid::flock(const std::vector<Boid>& boids, float sepW, float algnW, float cohW, float sepD, float algnD, float cohD)
{
    separate(boids, sepD, sepW);
    align(boids, algnD, algnW);
    cohesion(boids, cohD, cohW);
}

void Boid::separate(const std::vector<Boid>& boids, float distance, float weight)
{
    m_steeringForce = getSeparation(boids, distance);
			
    if ( weight != 1.0 )
    {
        m_steeringForce *= weight;
    }
    
    m_acceleration += m_steeringForce;
}

cocos2d::Vec3 Boid::getSeparation(const std::vector<Boid>& boids, float separation)
{
    cocos2d::Vec3 force;
    
    int count = 0;
    for (const auto& boid: boids) {
        auto distance = m_position.distance(boid.m_position);
        
        if (distance > 0 && distance < separation) {
            auto difference = m_position - boid.m_position;
            difference.normalize();
            difference *= 1/distance;
            force += difference;
            ++count;
        }
    }
    
    if (count > 0)
        force *= 1 / count;
    
    return force;
}

void Boid::align(const std::vector<Boid>& boids, float distance, float weight)
{
    m_steeringForce = getAlignment(boids, distance);
			
    if ( weight != 1.0 )
    {
        m_steeringForce *= weight;
    }
    
    m_steeringForce += m_steeringForce;
}

cocos2d::Vec3 Boid::getAlignment(const std::vector<Boid>& boids, float neighborDistance)
{
    cocos2d::Vec3 force;
    int count = 0;
    for (const auto& boid: boids) {
        auto distance = m_position.distance(boid.m_position);
        
        if ( distance > 0 && distance < neighborDistance )
        {
            force += (boid.m_velocity);
            count++;
        }
    }
    
    if (count > 0){
        force *=1 / count;
        if (force.lengthSquared() > m_maxForceSQ) {
            force.normalize();
            force *= m_maxForce;
        }
    }
			
    return force;
}

void Boid::cohesion(const std::vector<Boid>& boids, float distance, float weight)
{
    m_steeringForce = getCohesion(boids, distance);
			
    if ( weight != 1.0 )
    {
        m_steeringForce *= weight;
    }
    
    m_acceleration += m_steeringForce;
}

cocos2d::Vec3 Boid::getCohesion(const std::vector<Boid>& boids, float neighborDistance)
{
    cocos2d::Vec3 force;
    int count = 0;
    
    for (const auto& boid: boids) {
        auto distance = m_position.distance(boid.m_position);
        if (distance > 0 && distance < neighborDistance) {
            force += boid.m_position;
            ++count;
        }
    }
    
    if (count > 0) {
        force *= 1 / count;
        force = steer(force);
    }
    
    return force;
}

cocos2d::Vec3 Boid::steer(const cocos2d::Vec3& target, bool ease, float easeDistance)
{
    m_steeringForce = target;
    m_steeringForce -= m_position;
    
    m_distance = m_steeringForce.length();
    m_steeringForce.normalize();
    
    if (m_distance > 0.00001) {
        if (ease && m_distance <easeDistance) {
            m_steeringForce *= m_maxSpeed * (m_distance / easeDistance);
        } else {
            m_steeringForce *= m_maxSpeed;
        }
        
        m_steeringForce -= m_velocity;
        
        if (m_steeringForce.lengthSquared() > m_maxForceSQ) {
            m_steeringForce.normalize();
            m_steeringForce *= m_maxForceSQ;
        }
    }
    
    return m_steeringForce;
}

void Boid::update()
{
    m_oldPosition.x = m_position.x;
    m_oldPosition.y = m_position.y;
    m_oldPosition.z = m_position.z;
    
    m_velocity += m_acceleration;
    
    if ( m_velocity.lengthSquared() > m_maxSpeedSQ )
    {
        m_velocity.normalize();
        m_velocity *= m_maxSpeed;
    }
    
    m_position += m_velocity;
    
    m_acceleration.x = 0;
    m_acceleration.y = 0;
    m_acceleration.z = 0;
    
    if (m_boundsRadius == 0) {
        return;
    }
    
    if(m_position != m_oldPosition) {
        auto distance = m_position.distance(m_boundsCenter);
        
        if( distance > m_boundsRadius + m_radius )
        {
            switch(m_edgeBehavior)
            {
                case EdgeBehav::kEdgeNone :
                    return;
                case EdgeBehav::kEdgeBounce :
                
                    /**
                     * Move the boid to the edge of the boundary 
                     * then invert it's velocity and step it 
                     * forward back into the sphere 
                     */
                    
                    m_position -= m_boundsCenter;
                    m_position.normalize();
                    m_position *= m_boundsRadius + m_radius;
                    
                    m_velocity *= -1;
                    m_position += m_velocity;
                    m_position += m_boundsCenter;
                
                    break;
                
                case EdgeBehav::kEdgeWrap :
                    
                    /**
                     * Move the Boid to the antipodal point of it's 
                     * current position on the bounding sphere by 
                     * taking the inverse of it's position vector
                     */

                    m_position -= m_boundsCenter;
                    m_position.negate();
                    m_position += m_boundsCenter;

                    break;
            }
        }
    }
}

void Boid::constraintToRect(const cocos2d::Rect& rect, EdgeBehav behavior, float zMin, float zMax)
{
    //todo
}

void Boid::brake(float brakingForce)
{
    m_velocity *= 1 - brakingForce;
}

void Boid::seek(const cocos2d::Vec3& target, float multiplier)
{
    m_steeringForce = steer(target);
    
    if (multiplier != 1.0)
    {
        m_steeringForce *= multiplier;
    }
    
    m_acceleration += m_steeringForce;
}

void Boid::arrive(const cocos2d::Vec3& target, float easeDistance, float multiplier)
{
    m_steeringForce = steer(target, true, easeDistance);
    
    if ( multiplier != 1.0 )
    {
        m_steeringForce *= multiplier;
    }
    
    m_acceleration += m_steeringForce;
}

//
void Boid::flee(const cocos2d::Vec3& target, float panicDistance, float multiplier)
{
    m_distance = m_position.distance(target);
    
    if ( m_distance > panicDistance ) {
        return;
    }
    
    m_steeringForce = steer(target, true, -panicDistance);
    
    if ( multiplier != 1.0 )
    {
        m_steeringForce *= multiplier;
    }
    
    m_steeringForce.negate();
    m_acceleration += m_steeringForce;
}

void Boid::wander(float multiplier)
{
    std::random_device rd;
    
    std::default_random_engine engine(rd());
    std::uniform_real_distribution<float> distr(0.0f, 1.0f);

    m_wanderTheta += -m_wanderStep + distr(engine) * m_wanderStep * 2.0f;
    m_wanderPhi += -m_wanderStep + distr(engine) * m_wanderStep * 2.0f;
    m_wanderPsi += -m_wanderStep + distr(engine) * m_wanderStep * 2.0f;
    
    if ( distr(engine) < 0.5f )
    {
        m_wanderTheta = -m_wanderTheta;
    }
    
    auto pos = m_velocity;
    
    pos.normalize();
    pos *= m_wanderDistance;
    pos += m_position;

    cocos2d::Vec3 offset;
    
    
    offset.x = m_wanderRadius * std::cos(m_wanderTheta);
    offset.y = m_wanderRadius * std::sin(m_wanderPhi);
    offset.z = m_wanderRadius * std::cos(m_wanderPsi);
    
    pos.add(offset);
    m_steeringForce = steer(pos);
    
    if ( multiplier != 1.0 )
    {
        m_steeringForce *= multiplier;
    }
    
    m_acceleration += m_steeringForce;
}

/*
    // Add some wander to keep things interesting
    boid.wander(0.3);
    
    // Add a mild attraction to the centre to keep them on screen
    boid.seek(boid.boundsCentre, 0.1);
    
    // Flock
    boid.flock(_boids);
    
    boid.update();
    boid.render();

    boid.edgeBehavior = Boid.EDGE_BOUNCE;
    boid.maxForce = random(_config.minForce, _config.maxForce);
    boid.maxSpeed = random(_config.minSpeed, _config.maxSpeed);
    boid.wanderDistance = random(_config.minWanderDistance, _config.maxWanderDistance);
    boid.wanderRadius = random(_config.minWanderRadius, _config.maxWanderRadius);
    boid.wanderStep = random(_config.minWanderStep, _config.maxWanderStep);
    boid.boundsRadius = stage.stageWidth * 0.6;
    boid.boundsCentre = new Vector3D(stage.stageWidth >> 1, stage.stageHeight >> 1, 0.0);
    
    if(boid.x == 0 && boid.y == 0)
    {
        boid.x = boid.boundsCentre.x + random(-100, 100);
        boid.y = boid.boundsCentre.y + random(-100, 100);
        boid.z = random(-100, 100);
        
        var vel : Vector3D = new Vector3D(random(-2, 2), random(-2, 2), random(-2, 2));
        boid.velocity.incrementBy(vel);
    }
*/

