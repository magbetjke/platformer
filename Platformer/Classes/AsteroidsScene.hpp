//
//  AsteroidsScene.hpp
//  Platformer
//
//  Created by Алексей Еремин on 05/05/15.
//
//

#pragma once

#include "cocos2d.h"

class ConvexPoly;

class AsteroidsScene final : public cocos2d::Layer
{
public:
    CREATE_FUNC(AsteroidsScene)
    static cocos2d::Scene* createScene();
    
    ~AsteroidsScene();
    
    void update(float delta);
protected:
    bool init() override;
    void checkCollision(ConvexPoly* a, ConvexPoly* b);
    bool polyIntersects(ConvexPoly* a, ConvexPoly* b, cocos2d::Vec2& collisionNormal);
    bool axisSeparatePolygons(cocos2d::Vec2& normal, ConvexPoly* a, ConvexPoly* b, cocos2d::Vec2& collisionNormal, float& overlap);
    cocos2d::Vec2 findMTD(cocos2d::Vec2& push, int num);
    void calculateInterval(const cocos2d::Vec2& axis, ConvexPoly* poly, float& min, float& max);
    cocos2d::Vec2 findMTD(cocos2d::Vec2* push, int num);
    
private:
    cocos2d::DirectionLight* _light;
    cocos2d::AmbientLight* _ambient;
    
    std::vector<ConvexPoly*> _asteroids;

};