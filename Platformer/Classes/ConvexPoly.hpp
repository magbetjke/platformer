/**
 *
 * ConvexPoly.hpp
 *
 * @author a.eremin
 *
 */

#pragma once

#include "cocos2d.h"

class ConvexPoly : public cocos2d::Sprite3D
{
public:
    static ConvexPoly* create(int num);

    virtual ~ConvexPoly();
    
    cocos2d::Vec2 _velocity;
    
    float _radius;
        
    int _numVerticies;
    
    cocos2d::Vec3 getGlobalVertexPos(size_t i);
    
protected:
    ConvexPoly(int num);
    bool init() override;
    virtual void draw(cocos2d::Renderer *renderer, const cocos2d::Mat4 &transform, uint32_t flags) override;
    
    cocos2d::GLProgram* getOrCreateShader();
    virtual void buildMesh();
    virtual void applyShader();

    cocos2d::Mesh* _mesh;
    cocos2d::MeshCommand _meshCommand;
    GLenum _primitiveType;
    
    std::vector<cocos2d::Vec3> _verticies;
    
};