uniform vec3 u_DirLightSourceColor[1];
uniform vec3 u_DirLightSourceDirection[1];
uniform float u_receiver_y;

attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec3 a_normal;
varying vec2 v_texture_coord;
varying vec3 v_normal;

void main(void)
{
    vec4 position = CC_MVMatrix * a_position;

    vec3 plane_normal = vec3(0.0, 1.0, 0.0);
    vec3 light_direction = normalize(u_DirLightSourceDirection[0]);

    float temp = ((dot(plane_normal, position.xyz) - u_receiver_y) / dot(plane_normal, light_direction.xyz));

    position.x -= temp * light_direction.x;
    position.y = u_receiver_y;
    position.z -= temp * light_direction.z;

    gl_Position = CC_PMatrix * position;
    gl_PointSize = 10.0;
    v_texture_coord = a_texCoord;
    v_texture_coord.y = (1.0 - v_texture_coord.y);
	v_normal = CC_NormalMatrix * a_normal;
}
